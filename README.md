# Test technique - EXOMIND

## Sommaire
1. Instructions
2. Le code et son organisation

---

## Instructions
L'application doit charger une liste d'albums à partir d'un point d'API REST.

Les données chargées doivent être affichées à l'utilisateur et persister à fin d'être disponibles dans les conditions suivantes :
- accès _offline_;
- changement de configuration (multi-screen); 
- redémarrage de l'application ou du téléphone;
- restauration de l'application;

## Le code et son organisation

### Langage
L'application sera intégralement codée en Kotlin et se reposera RxKotlin :
- Kotlin, car ce langage est plus clair et plus agréable à utiliser que Java 7 & 8.
- RxKotlin, car elle permet une gestion de l'asynchrone plus maintenable et plus sûre.

### Android Studio
Un Android Studio avec version minimale `3.6.3` est nécessaire pour la compilation du projet.

### Front-End
L'application utilise le pattern MVVM à l'aide de la stack AndroidX (Jetpack - Architecture Components) :
- `Lifecycle`: pour la gestion et le respect des cycles de vie de l'app et de ses vues (Activities & Fragments);
- `ViewModel` et `LiveData`: pour une gestion propre des données en-dehors de la vue, en respect de son cycle de vie et ainsi permettre le DataBinding;
- `Navigation`: pour simplifier la navigation dans l'app avec ses Fragments;
- `Room`: pour la sauvegarde des données chargées dans une base de données locale SQL;

### Back-End
Pour la partie communication avec le point d'API REST, l'application utilise `Retrofit 2` qui se chargera de récupérer les données proprement.
En raison du support d'Android SDK 19, une version dite _legacy_ de `okhttp` doit être utilisée car les nouvelles versions supportent uniquement Android SDK 21.

### Architecture du projet Android
Le projet est composé de trois modules:
1. le module `feature` qui contient toute la partie applicative, à savoir les vues qui seront affichées (UI /UX).
2. le module `domain` servant à stocker les données chargées. Ce module reste indépendant.
3. le module `data` servant à communiquer avec le point d'API.
4. le module `common` qui contient principalement du code dit utilitaire. Ce module reste indépendant.
4. le module `di` servant à assurer l'injection de dépendance.


Une telle modularisation facilite la maintenance du code, par la séparation des logiques, et une compilation plus rapide avec Gradle.

### Autres librairies / dépendances
L'application fera appel aux librairies suivantes pour son bon fonctionnement :
1. `Dagger 2` pour l'injection des APIs du module`service` comme dépendances dans les parties du module `app` concernées.
2. `Glide` pour le chargement et la mise en cache des images de façon asynchrone.
3. `OkHTTP 3.12.x` pour le support en mode _legacy_ des SDK antérieures à 21
