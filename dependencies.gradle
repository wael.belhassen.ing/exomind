allprojects {
    repositories {
        jcenter()
        google()
    }
}

ext {
    //ApplicationId
    appId = "com.bwael.exomind"

    //Android
    androidMinSdkVersion = 21
    androidTargetSdkVersion = 29
    androidCompileSdkVersion = 29

    //Libraries
    kotlinVersion = '1.3.72'
    kotlinKtx = '1.3.0'
    rxKotlinVersion = '2.3.0'
    rxAndroidVersion = '2.1.1'
    javaxAnnotationVersion = '1.0'
    javaxInjectVersion = '1'
    okHttpVersion = '4.7.2'
    okHttpUrlconnection = '4.7.2'
    retrofitVersion = '2.9.0'
    roomVersion = '2.2.5'
    timberVersion = '4.7.1'
    glideVersion = '4.11.0'
    daggerVersion = '2.28.1'
    jackson = '2.11.0'
    jacksonConverter = '2.9.0'

    //Testing
    kotlin_version_JUnit = '1.3.72'
    jUnitVersion = '4.13'
    jUnitVersionExt = '1.1.1'
    coreTest = '2.1.0'
    assertJVersion = '3.8.0'
    espressoVersion = '3.0.0'
    mockitoKotlinVersion = '2.2.0'
    mockitoInline = '2.8.47'
    mockitoAndroidVersion = '3.1.0'
    androidSupportRunnerVersion = '1.3.0-rc01'
    androidSupportRulesVersion = '1.3.0-rc01'
    runnerVersion = '0.5'
    espressoCore = '3.2.0'
    espressoIntents = '3.2.0'
    espressoContrib = '3.2.0'

    // Debug DataBase
    debugDataBase = '1.0.3'

    // autoDispose
    autodisposeAndroid = '1.4.0'
    autodisposeAndroidArchcomponents = '1.4.0'
    autodisposeKotlin = '0.8.0'

    //EventBus
    eventbus = '3.1.1'

    //Lottie
    lottie = '3.4.1'

    //multidex
    multidex = '2.0.1'

    //Fabric
    fabric = '2.10.1'

    //RxBinding
    rxBinding = '2.2.0'

    //CircularImage
    circularImage = '3.1.0'

    // PersistentCookieJar
    persistentCookieJar = 'v1.0.1'

    // Stomp Socket
    stompSocket = '1.6.4'

    //Firebase push notification
    fireBaseNotification = '20.2.1'
    fireBaseAnalytics = '17.4.3'
    fireBaseCrashlytics = '17.1.0'
    fireBaseCore = '17.4.3'
    fireBaseVision = '24.0.3'
    googleMaps = '17.0.0'
    googleMapsUtils = '2.0.0'
    googlePlayServcieLocation = '17.0.0'

    // Shimmer facebook placeholder
    layoutFacebookPlaceHolder = '0.5.0'

    // lifecycle
    archLifecyle = '2.2.0'

    // liveData Ktx
    archLiveDataKtx = '2.2.0'

    // android annotations
    androidAnnotations = '1.1.0'

    // legacy support
    androidLegacySupport = '1.0.0'

    // appCompat
    appCompat = '1.1.0'

    // recyclerView
    recyclerView = '1.1.0'

    // material Design
    materialDesign = '1.1.0 '

    // cardView
    cardView = '1.0.0'
    Carbon = '0.16.0.1'

    // constraintLayout
    constraintLayout = '2.0.0-beta7'

    //CameraX
    cameraXCore = '1.0.0-alpha03'
    cameraX = '1.0.0-alpha03'
    cameraXLifecycle = '1.0.0-alpha03'
    cameraXView = '1.0.0-alpha03'
    cameraExtension = '1.0.0-alpha03'
    exifCameraX = '1.1.0'

    //utils
    circleimageview = '3.0.2'
    pinView = '1.4.3'
    pageIndiocator = '1.0.3'
    switchIosLike = '0.0.3@aar'
    timeHelper = '1.2.4'

    mobileUiDependencies = [
            multidex                        : "androidx.multidex:multidex:${multidex}",
            archRuntime                     : "androidx.lifecycle:lifecycle-runtime:${archLifecyle}",
            archExtensions                  : "androidx.lifecycle:lifecycle-extensions:${archLifecyle}",
            archCompiler                    : "androidx.lifecycle:lifecycle-compiler:${archLifecyle}",
            archLiveDataKtx                 : "androidx.lifecycle:lifecycle-livedata-ktx:${archLiveDataKtx}",

            androidAnnotations              : "androidx.annotation:annotation:${androidAnnotations}",
            androidLegacySupport            : "androidx.legacy:legacy-support-v4:${androidLegacySupport}",
            appCompat                       : "androidx.appcompat:appcompat:${appCompat}",
            recyclerView                    : "androidx.recyclerview:recyclerview:${recyclerView}",
            cardView                        : "androidx.cardview:cardview:${cardView}",
            materialDesign                  : "com.google.android.material:material:${materialDesign}",
            constraintLayout                : "androidx.constraintlayout:constraintlayout:${constraintLayout}",
            daggerCompiler                  : "com.google.dagger:dagger-compiler:${daggerVersion}",
            dagger                          : "com.google.dagger:dagger:${daggerVersion}",
            rxKotlin                        : "io.reactivex.rxjava2:rxkotlin:${rxKotlinVersion}",
            rxAndroid                       : "io.reactivex.rxjava2:rxandroid:${rxAndroidVersion}",
            glide                           : "com.github.bumptech.glide:glide:${glideVersion}",
            glideCompiler                   : "com.github.bumptech.glide:compiler:${glideVersion}",
            kotlin                          : "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${kotlinVersion}",
            kotlinKtx                       : "androidx.core:core-ktx:${kotlinKtx}",
            javaxAnnotation                 : "javax.annotation:jsr250-api:${javaxAnnotationVersion}",
            javaxInject                     : "javax.inject:javax.inject:${javaxInjectVersion}",
            timber                          : "com.jakewharton.timber:timber:${timberVersion}",
            daggerSupport                   : "com.google.dagger:dagger-android-support:${daggerVersion}",
            daggerProcessor                 : "com.google.dagger:dagger-android-processor:${daggerVersion}",
            roomRuntime                     : "androidx.room:room-runtime:${roomVersion}",
            roomCompiler                    : "androidx.room:room-compiler:${roomVersion}",
            roomRxJava                      : "androidx.room:room-rxjava2:${roomVersion}",

            junit                           : "junit:junit:${jUnitVersion}",
            junitExt                        : "androidx.test.ext:junit:${jUnitVersionExt}",
            coreTest                        : "androidx.arch.core:core-testing:${coreTest}",
            kotlinJUnit                     : "org.jetbrains.kotlin:kotlin-test-junit:${kotlin_version_JUnit}",
            assertj                         : "org.assertj:assertj-core:${assertJVersion}",
            mockito                         : "com.nhaarman.mockitokotlin2:mockito-kotlin:${mockitoKotlinVersion}",
            mockitoInline                   : "org.mockito:mockito-inline:${mockitoInline}",
            mockitoAndroid                  : "org.mockito:mockito-android:${mockitoAndroidVersion}",
            mockitoCore                     : "org.mockito:mockito-core:${mockitoAndroidVersion}",
            espressoCore                    : "androidx.test.espresso:espresso-core:${espressoCore}",
            espressoIntents                 : "androidx.test.espresso:espresso-intents:${espressoIntents}",
            espressoContrib                 : "androidx.test.espresso:espresso-contrib:${espressoContrib}",
            supportRunner                   : "androidx.test:runner:${androidSupportRunnerVersion}",
            androidRules                    : "androidx.test:rules:${androidSupportRulesVersion}",

            okHttp                          : "com.squareup.okhttp3:okhttp:${okHttpVersion}",
            okHttpUrlconnection             : "com.squareup.okhttp3:okhttp-urlconnection:${okHttpUrlconnection}",
            okHttpLogger                    : "com.squareup.okhttp3:logging-interceptor:${okHttpVersion}",
            retrofit                        : "com.squareup.retrofit2:retrofit:${retrofitVersion}",
            retrofitConverter               : "com.squareup.retrofit2:converter-gson:${retrofitVersion}",
            retrofitAdapter                 : "com.squareup.retrofit2:adapter-rxjava2:${retrofitVersion}",

            autodisposeAndroid              : "com.uber.autodispose:autodispose-android:${autodisposeAndroid}",
            autodisposeAndroidArchcomponents: "com.uber.autodispose:autodispose-android-archcomponents:${autodisposeAndroidArchcomponents}",
            autodisposeKotlin               : "com.uber.autodispose:autodispose-kotlin:${autodisposeKotlin}",

            jacksonCore                     : "com.fasterxml.jackson.core:jackson-core:${jackson}",
            jacksonAnnotations              : "com.fasterxml.jackson.core:jackson-annotations:${jackson}",
            jacksonDataBind                 : "com.fasterxml.jackson.core:jackson-databind:${jackson}",
            jacksonConverter                : "com.squareup.retrofit2:converter-jackson:${jacksonConverter}",

            debugDataBase                   : "com.amitshekhar.android:debug-db:${debugDataBase}",

            eventbus                        : "org.greenrobot:eventbus:${eventbus}",

            lottie                          : "com.airbnb.android:lottie:${lottie}",

            fireBaseNotification            : "com.google.firebase:firebase-messaging:${fireBaseNotification}",
            fireBaseAnalytics               : "com.google.firebase:firebase-analytics:${fireBaseAnalytics}",
            fireBaseCrashlytics             : "com.google.firebase:firebase-crashlytics:${fireBaseCrashlytics}",
            fireBaseCore                    : "com.google.firebase:firebase-core:${fireBaseCore}",
            fireBaseVision                  : "com.google.firebase:firebase-ml-vision:${fireBaseVision}",
            googleMaps                      : "com.google.android.gms:play-services-maps:${googleMaps}",
            googleMapsUtils                 : "com.google.maps.android:android-maps-utils:${googleMapsUtils}",
            googlePlayServcieLocation       : "com.google.android.gms:play-services-location:${googlePlayServcieLocation}",

            rxbindingSupport                : "com.jakewharton.rxbinding2:rxbinding-support-v4:${rxBinding}",
            rxbindingAppcompat              : "com.jakewharton.rxbinding2:rxbinding-appcompat-v7:${rxBinding}",
            rxbindingDesign                 : "com.jakewharton.rxbinding2:rxbinding-design:${rxBinding}",
            rxbindingRecyclerView           : "com.jakewharton.rxbinding2:rxbinding-recyclerview-v7:${rxBinding}",

            shimmerEffect                   : "com.facebook.shimmer:shimmer:${layoutFacebookPlaceHolder}",
            circleimageview                 : "de.hdodenhof:circleimageview:${circularImage}",
            Carbon                          : "tk.zielony:carbon:${Carbon}",

            cameraX                         : "androidx.camera:camera-camera2:${cameraX}",
            cameraXCore                     : "androidx.camera:camera-core:${cameraXCore}",
            cameraXLifecycle                : "androidx.camera:camera-lifecycle:${cameraXLifecycle}",
            cameraXView                     : "androidx.camera:camera-view:${cameraXView}",
            cameraExtension                 : "androidx.camera:camera-extensions:${cameraXView}",
            exifCameraX                     : "androidx.exifinterface:exifinterface:${exifCameraX}",


            pinView                         : "com.chaos.view:pinview:${pinView}",
            pageIndiocator                  : "com.romandanylyk:pageindicatorview:${pageIndiocator}",
            switchIosLike                   : "com.github.zcweng:switch-button:${switchIosLike}",
            timeHelper                      : "com.jakewharton.threetenabp:threetenabp:${timeHelper}"
    ]
}