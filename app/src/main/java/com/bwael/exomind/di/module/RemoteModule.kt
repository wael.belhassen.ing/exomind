package com.bwael.exomind.di.module

import com.bwael.exomind.data.remote.service.NetworkModuleFactory
import com.bwael.exomind.data.remote.service.endpoints.UserServiceEndPoint
import dagger.Module
import dagger.Provides

@Module
class RemoteModule {

    @Provides
    internal fun provideUserServiceEndPoint(): UserServiceEndPoint =
        NetworkModuleFactory.makeUserServiceEndPoint()
}
