package com.bwael.exomind.di.component

import android.content.Context
import com.bwael.exomind.common.archi.activity.BaseDataBindingActivity
import com.bwael.exomind.common.archi.activity.CommonBaseActivityBehaviour
import com.bwael.exomind.common.archi.dialog.BaseDialogFragment
import com.bwael.exomind.common.archi.fragment.BaseFragment
import com.bwael.exomind.di.module.ContractModule
import com.bwael.exomind.di.module.RemoteModule
import com.bwael.exomind.di.module.ViewModelModule
import com.bwael.exomind.di.subcomponent.home.HomeSubComponent
import com.bwael.exomind.di.subcomponent.movie.MovieSubComponent
import com.bwael.exomind.features.main.MainActivity
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

// Scope annotation that the AppComponent uses
// Classes annotated with @Singleton will have a unique instance in this Component
@Singleton
// Definition of a Dagger component that adds info from the different modules to the graph
@Component(
    modules = [
        RemoteModule::class,
        ViewModelModule::class,
        ContractModule::class
    ]
)
interface ApplicationComponent {

    // Factory to create instances of the AppComponent
    @Component.Factory
    interface Factory {
        // With @BindsInstance, the Context passed in will be available in the graph
        fun create(@BindsInstance context: Context): ApplicationComponent
    }

    // Types that can be retrieved from the graph
    fun homeSubComponent(): HomeSubComponent.Factory
    fun movieSubComponent(): MovieSubComponent.Factory

    fun inject(baseDataBindingActivity: BaseDataBindingActivity)
    fun inject(commonBaseActivityBehaviour: CommonBaseActivityBehaviour)
    fun inject(baseFragment: BaseFragment)
    fun inject(baseDialogFragment: BaseDialogFragment)

    fun inject(mainActivity: MainActivity)
}
