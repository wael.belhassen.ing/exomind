package com.bwael.exomind.di.module

import com.bwael.exomind.data.remote.datasource.UserRemote
import com.bwael.exomind.data.remote.datasource.UserRemoteImp
import com.bwael.exomind.data.repository.user.UserRepository
import com.bwael.exomind.data.repository.user.UserRepositoryImp
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class ContractModule {
    @Binds
    @Singleton
    abstract fun getUserRemoteImp(userRemoteImp: UserRemoteImp): UserRemote

    @Binds
    @Singleton
    abstract fun getUserRepositoryImp(userRepositoryImp: UserRepositoryImp): UserRepository
}
