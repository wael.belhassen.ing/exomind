package com.bwael.exomind.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bwael.exomind.common.archi.viewmodel.fatory.ViewModelFactory
import com.bwael.exomind.common.archi.viewmodel.fatory.ViewModelKey
import com.bwael.exomind.common.dialogs.alertdialog.CustomDialogViewModel
import com.bwael.exomind.features.main.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Singleton

@Module
abstract class ViewModelModule {
    @Binds
    @Singleton
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @Singleton
    @ViewModelKey(CustomDialogViewModel::class)
    abstract fun bindsCustomDialogViewModel(customDialogViewModel: CustomDialogViewModel): ViewModel

    @Binds
    @IntoMap
    @Singleton
    @ViewModelKey(MainViewModel::class)
    abstract fun bindsMainViewModel(customDialogViewModel: MainViewModel): ViewModel
}
