package com.bwael.exomind.di.subcomponent.home

import dagger.Subcomponent

@Subcomponent
interface HomeSubComponent {
    // Factory to create instances of HomeSubComponent
    @Subcomponent.Factory
    interface Factory {
        fun create(): HomeSubComponent
    }

//    fun inject(homeActivity: HomeActivity)
}
