package com.bwael.exomind.di.subcomponent.movie

import dagger.Subcomponent

@Subcomponent
interface MovieSubComponent {
    // Factory to create instances of MovieSubComponent
    @Subcomponent.Factory
    interface Factory {
        fun create(): MovieSubComponent
    }

//    fun inject(homeActivity: HomeActivity)
}
