package com.bwael.exomind.domain

import com.bwael.exomind.common.archi.usecase.BaseUseCaseOut
import com.bwael.exomind.data.entities.UserObject
import com.bwael.exomind.data.repository.user.UserRepository
import io.reactivex.Single
import javax.inject.Inject

class RetrieveUsersUseCase @Inject constructor() :
    BaseUseCaseOut<Single<MutableList<UserObject?>?>>() {

    @Inject
    lateinit var userRepository: UserRepository

    override fun execute(): Single<MutableList<UserObject?>?> = userRepository.getUsers()
}
