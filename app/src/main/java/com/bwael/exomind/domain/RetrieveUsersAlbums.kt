package com.bwael.exomind.domain

import com.bwael.exomind.common.archi.usecase.BaseUseCaseInOut
import com.bwael.exomind.data.entities.UserAlbum
import com.bwael.exomind.data.repository.user.UserRepository
import io.reactivex.Single
import javax.inject.Inject

class RetrieveUsersAlbums @Inject constructor() :
    BaseUseCaseInOut<Int, Single<MutableList<UserAlbum>?>>() {

    @Inject
    lateinit var userRepository: UserRepository

    override fun execute(parameter: Int): Single<MutableList<UserAlbum>?> =
        userRepository.getUsersAlbums(parameter)
}
