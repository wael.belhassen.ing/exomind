package com.bwael.exomind.domain

import com.bwael.exomind.common.archi.usecase.BaseUseCaseInOut
import com.bwael.exomind.data.entities.UserAlbumImage
import com.bwael.exomind.data.repository.user.UserRepository
import io.reactivex.Single
import javax.inject.Inject

class RetrieveUsersAlbumsImage @Inject constructor() :
    BaseUseCaseInOut<Pair<Int?, Int?>, Single<MutableList<UserAlbumImage>?>>() {

    @Inject
    lateinit var userRepository: UserRepository

    override fun execute(parameter: Pair<Int?, Int?>): Single<MutableList<UserAlbumImage>?> =
        userRepository.getUsersAlbumsImage(
            parameter.first,
            parameter.second
        ) // first = id, second = albumIds
}
