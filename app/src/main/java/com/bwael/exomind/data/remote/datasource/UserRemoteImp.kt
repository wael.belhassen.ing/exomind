package com.bwael.exomind.data.remote.datasource

import com.bwael.exomind.data.entities.UserAlbum
import com.bwael.exomind.data.entities.UserAlbumImage
import com.bwael.exomind.data.entities.UserObject
import com.bwael.exomind.data.remote.service.endpoints.UserServiceEndPoint
import io.reactivex.Single
import javax.inject.Inject

class UserRemoteImp @Inject constructor() : UserRemote {

    @Inject
    lateinit var serviceEndPoint: UserServiceEndPoint

    override fun getUsers(): Single<MutableList<UserObject?>?> = serviceEndPoint.getUsers()

    override fun getUsersAlbums(idUser: Int?): Single<MutableList<UserAlbum>?> =
        serviceEndPoint.getUsersAlbums(idUser)

    override fun getUsersAlbumsImage(
        idUser: Int?,
        albumId: Int?
    ): Single<MutableList<UserAlbumImage>?> = serviceEndPoint.getUsersAlbumsImage(idUser, albumId)
}
