package com.bwael.exomind.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
open class Movie {
    @PrimaryKey
    var id: Long = 0
    @JsonProperty("poster_path")
    var posterPath: String? = null
    @JsonProperty("adult")
    var isAdult: Boolean = false
    @JsonProperty("overview")
    var overview: String? = null
    @JsonProperty("release_date")
    var releaseDate: String? = null
    @JsonProperty("genre_ids")
    var genreIds: List<Long>? = null
    @JsonProperty("original_title")
    var originalTitle: String? = null
    @JsonProperty("original_language")
    var originalLanguage: String? = null
    @JsonProperty("title")
    var title: String? = null
    @JsonProperty("backdrop_path")
    var backdropPath: String? = null
    @JsonProperty("popularity")
    var popularity: Double = 0.toDouble()
    @JsonProperty("vote_count")
    var voteCount: Long = 0
    @JsonProperty("video")
    var isVideo: Boolean = false
    @JsonProperty("vote_average")
    var voteAverage: Double = 0.toDouble()

    init {
        genreIds = ArrayList()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Movie) return false
        val movie = other as Movie?
        return id == movie?.id
    }

    override fun hashCode(): Int = (id xor id.ushr(BITCOUNT)).toInt()

    override fun toString(): String {
        return "Movie(" +
                "\n    id=$id," +
                "\n    posterPath=$posterPath," +
                "\n    isAdult=$isAdult," +
                "\n    overview=$overview," +
                "\n    releaseDate=$releaseDate," +
                "\n    genreIds=$genreIds," +
                "\n    originalTitle=$originalTitle," +
                "\n    originalLanguage=$originalLanguage," +
                "\n    title=$title," +
                "\n    backdropPath=$backdropPath," +
                "\n    popularity=$popularity," +
                "\n    voteCount=$voteCount," +
                "\n    isVideo=$isVideo," +
                "\n    voteAverage=$voteAverage" +
                "\n)"
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    class Dates {

        @JsonProperty("maximum")
        var maximum: String? = null
        @JsonProperty("minimum")
        var minimum: String? = null

        override fun toString(): String {
            var data = ""
            data += "Maximum[$maximum]"
            data += "Minimum[$minimum]"
            data += "\n"
            return data
        }
    }

    companion object {
        private const val BITCOUNT = 32
    }
}
