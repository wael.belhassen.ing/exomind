package com.bwael.exomind.data.entities

class UserAlbum {
    var userId: Int? = null
    var id: Int? = null
    var title: String? = null

    override fun toString(): String = "UserAlbum(userId=$userId, id=$id, title=$title)"
}
