package com.bwael.exomind.data.repository.user

import com.bwael.exomind.data.entities.UserAlbum
import com.bwael.exomind.data.entities.UserAlbumImage
import com.bwael.exomind.data.entities.UserObject
import com.bwael.exomind.data.remote.datasource.UserRemote
import io.reactivex.Single
import javax.inject.Inject

class UserRepositoryImp @Inject constructor() : UserRepository {

    @Inject
    lateinit var userRemote: UserRemote

    override fun getUsers(): Single<MutableList<UserObject?>?> = userRemote.getUsers()

    override fun getUsersAlbums(idUser: Int?): Single<MutableList<UserAlbum>?> =
        userRemote.getUsersAlbums(idUser)

    override fun getUsersAlbumsImage(
        idUser: Int?,
        albumId: Int?
    ): Single<MutableList<UserAlbumImage>?> = userRemote.getUsersAlbumsImage(idUser, albumId)
}
