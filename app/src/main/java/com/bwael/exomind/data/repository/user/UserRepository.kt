package com.bwael.exomind.data.repository.user

import com.bwael.exomind.data.entities.UserAlbum
import com.bwael.exomind.data.entities.UserAlbumImage
import com.bwael.exomind.data.entities.UserObject
import io.reactivex.Single

interface UserRepository {
    fun getUsers(): Single<MutableList<UserObject?>?>

    fun getUsersAlbums(idUser: Int?): Single<MutableList<UserAlbum>?>

    fun getUsersAlbumsImage(idUser: Int?, albumId: Int?): Single<MutableList<UserAlbumImage>?>
}
