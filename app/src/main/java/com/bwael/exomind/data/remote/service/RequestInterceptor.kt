package com.bwael.exomind.data.remote.service

import android.annotation.SuppressLint
import okhttp3.*
import java.io.IOException

class RequestInterceptor internal constructor() : Interceptor {

    @SuppressLint("LogConditional")
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val headers = provideHeaders()
        val httpUrl = provideHttpUrl(request)
        val newRequest = provideRequest(request, headers, httpUrl)
        return chain.proceed(newRequest)
    }

    companion object {

        private fun provideRequest(original: Request, headers: Headers, httpUrl: HttpUrl): Request {
            val requestBuilder = original.newBuilder()
                .headers(headers)
                .url(httpUrl)
                .method(original.method, original.body)
            return requestBuilder.build()
        }

        /**
         * Provide headers with token as authorisation
         *
         * @param token the token
         * @return the headers
         */
        fun provideHeaders(): Headers {
            val headersBuilder = Headers.Builder()
            return headersBuilder.build()
        }

        private fun provideHttpUrl(original: Request): HttpUrl {
            val httpUrlBuilder = original.url.newBuilder()
            return httpUrlBuilder.build()
        }
    }
}
