package com.bwael.exomind.data.entities

class UserObject {
    var id: Int? = null
    var name: String? = null
    var username: String? = null
    var email: String? = null
    var phone: String? = null
    var website: String? = null
    var address: Address? = null
    override fun toString(): String =
        "UserObject(" +
                "\nid=$id," +
                "\nname=$name," +
                "\nusername=$username," +
                "\nemail=$email," +
                "\nphone=$phone," +
                "\nwebsite=$website," +
                "\naddress=$address" +
                "\n)"
}
