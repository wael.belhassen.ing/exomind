package com.bwael.exomind.data.remote.service

import com.bwael.exomind.BuildConfig
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import okhttp3.Interceptor
import okhttp3.JavaNetCookieJar
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.greenrobot.eventbus.EventBus
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import java.net.CookieManager
import java.net.CookiePolicy
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import javax.inject.Named

open class BaseNetworkModuleFactory {
    companion object {
        val CONNECT_TIMEOUT by lazy {
            60L // 10MB
        }
        val READ_TIMEOUT by lazy {
            60L // 10MB
        }
        val SUCCESS_RESPONSE_CODE by lazy {
            200
        }
    }

    fun makeOkHttpClient(): OkHttpClient {
        val cookieManager = CookieManager()
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL)
        val cookieJar = JavaNetCookieJar(cookieManager)

        return OkHttpClient.Builder()
            .addInterceptor(Interceptor { chain ->
                val request = chain.request()
                val response = chain.proceed(request)
                if (response.code != SUCCESS_RESPONSE_CODE) {
                    EventBus.getDefault().post("this is not a successful response")
                    return@Interceptor response
                }
                response
            })
            .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
            .cookieJar(cookieJar)
            .addInterceptor(provideLogInterceptor())
            .addInterceptor(provideRequestInterceptor())
            .build()
    }

    @Named(NetworkModuleFactory.LOG_INTERCEPTOR)
    internal fun provideLogInterceptor(): Interceptor {
        val logInterceptor = HttpLoggingInterceptor()
        logInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return logInterceptor
    }

    @Named(NetworkModuleFactory.REQUEST_INTERCEPTOR)
    internal fun provideRequestInterceptor(): Interceptor =
        RequestInterceptor()

    fun provideObjectMapper(): ObjectMapper {
        val objectMapper = ObjectMapper()
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        objectMapper.configure(JsonParser.Feature.AUTO_CLOSE_SOURCE, true)
        return objectMapper
    }

    ///////////////////////////////////////////////////////////////////////////
    // RETROFIT BUILDER
    ///////////////////////////////////////////////////////////////////////////
    fun buildRetrofitObject(
        okHttpClient: OkHttpClient,
        mapper: ObjectMapper
    ): Retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.BASEURL)
        .client(okHttpClient)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(JacksonConverterFactory.create(mapper))
        .callbackExecutor(Executors.newSingleThreadExecutor())
        .build()
}
