package com.bwael.exomind.data.constante

const val EMAIL_PATTERN =
    "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
const val NAME_REGEX = "([^0-9 ]+[ ]?)+"
const val CAGNOTTE_REGEX = "([a-zA-Z ]+[ ]?)+"
const val CARD_REGEX = "([a-zA-Z]?)+"
const val NUM_REGEX = "^[a-zA-Z0-9]*\$"

const val EMAIL_REGEX_PATTERN =
    "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
const val PASSWORD_REGEX_PATTERN = "^(\\d)(?!\\1+\$)\\d{5}\$"
const val LOGIN_RESPONSE_OBJECT = "LOGIN_RESPONSE_OBJECT"
const val USER_OBJECT_RESPONSE_OBJECT = "USER_OBJECT_RESPONSE_OBJECT"
const val PASSWORD_LENGTH = 6
const val SHARED_PREFERNCES_ONBORDING = "onBoarding"
const val ON_BOARDING_OPENED: Boolean = false

/** Divers **/
const val NUMBERS = 10

/** Image Extension **/
const val PHOTO_EXTENSION = ".jpg"

const val SHOULD_MOVE_TO_HOME_ON_RESULT_REQUEST_CODE = 1000
const val SHOULD_MOVE_TO_HOME_ON_RESULT_RESULT_CODE = 1001
const val SHOULD_MOVE_TO_HOME_ON_RESULT_VALUE = "SHOULD_MOVE_TO_HOME_ON_RESULT_VALUE"

const val USER_DEFAULT = "USER_DEFAULT"
