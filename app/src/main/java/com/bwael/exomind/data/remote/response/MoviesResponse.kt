package com.bwael.exomind.data.remote.response

import com.bwael.exomind.data.entities.Movie
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
class MoviesResponse {

    @JsonProperty("page")
    var page: Long = 0
    @JsonProperty("results")
    var movies: ArrayList<Movie> = ArrayList()

    init {
        movies = ArrayList()
    }

    override fun toString(): String {
        var data = ""
        data += "Page[$page]"
        //data += "\n";
        data += "Movies[$movies]"
        data += "\n"
        return data
    }
}
