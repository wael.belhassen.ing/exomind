package com.bwael.exomind.data.entities

class UserAlbumImage {
    var id: Int? = null
    var albumId: Int? = null
    var url: String? = null
    var thumbnailUrl: String? = null

    override fun toString(): String =
        "UserAlbumImage(id=$id, albumId=$albumId, url=$url, thumbnailUrl=$thumbnailUrl)"
}
