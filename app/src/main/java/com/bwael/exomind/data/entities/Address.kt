package com.bwael.exomind.data.entities

class Address {
    var street: String? = null
    var suite: String? = null
    var city: String? = null
    var zipcode: String? = null
}
