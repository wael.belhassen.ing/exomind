package com.bwael.exomind.data.remote.datasource

import com.bwael.exomind.data.entities.UserAlbum
import com.bwael.exomind.data.entities.UserAlbumImage
import com.bwael.exomind.data.entities.UserObject
import io.reactivex.Single

interface UserRemote {
    fun getUsers(): Single<MutableList<UserObject?>?>

    fun getUsersAlbums(idUser: Int?): Single<MutableList<UserAlbum>?>

    fun getUsersAlbumsImage(idUser: Int?, albumId: Int?): Single<MutableList<UserAlbumImage>?>
}
