package com.bwael.exomind.data.remote.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
class HomeDataResponse {
    var status: Boolean = false
    var data: ArrayList<Any>? = null

    override fun toString(): String = "HomeDataResponse(status=$status, data=$data)"
}
