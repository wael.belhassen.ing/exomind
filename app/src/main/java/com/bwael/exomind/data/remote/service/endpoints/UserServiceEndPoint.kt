package com.bwael.exomind.data.remote.service.endpoints

import com.bwael.exomind.data.entities.UserAlbum
import com.bwael.exomind.data.entities.UserAlbumImage
import com.bwael.exomind.data.entities.UserObject
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface UserServiceEndPoint {
    companion object {
        const val USER_URL = "/users/"
        const val USER_ALBUM_URL = "/users/{idUser}/albums"
        const val USER_ALBUM_IMAGE_URL = "/users/{idUser}/photos"

        /*Params*/
        const val ID_USER = "idUser"
        const val ID_ALBUM = "albumId"
    }

    @GET(USER_URL)
    fun getUsers(): Single<MutableList<UserObject?>?>

    @GET(USER_ALBUM_URL)
    fun getUsersAlbums(@Path(ID_USER) idUser: Int?): Single<MutableList<UserAlbum>?>

    @GET(USER_ALBUM_IMAGE_URL)
    fun getUsersAlbumsImage(
        @Path(ID_USER) idUser: Int?,
        @Query(ID_ALBUM) albumId: Int?
    ): Single<MutableList<UserAlbumImage>?>
}
