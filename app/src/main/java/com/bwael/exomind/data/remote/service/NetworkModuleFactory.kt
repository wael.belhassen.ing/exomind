package com.bwael.exomind.data.remote.service

import com.bwael.exomind.data.remote.service.endpoints.UserServiceEndPoint
import com.fasterxml.jackson.databind.ObjectMapper
import okhttp3.OkHttpClient

object NetworkModuleFactory : BaseNetworkModuleFactory() {

    const val LOG_INTERCEPTOR = "LogInterceptor"
    const val REQUEST_INTERCEPTOR = "RequestInterceptor"

    fun makeUserServiceEndPoint(): UserServiceEndPoint =
        makeUserServiceEndPoint(makeOkHttpClient(), provideObjectMapper())

    private fun makeUserServiceEndPoint(
        okHttpClient: OkHttpClient,
        mapper: ObjectMapper
    ): UserServiceEndPoint {
        val retrofit = buildRetrofitObject(okHttpClient, mapper)
        return retrofit.create(UserServiceEndPoint::class.java)
    }
}
