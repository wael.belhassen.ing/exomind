package com.bwael.exomind.features.user

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.bwael.exomind.R
import com.bwael.exomind.common.archi.fragment.BaseFragment
import com.bwael.exomind.common.archi.resources.ResourceState
import com.bwael.exomind.common.dialogs.DialogUtils
import com.bwael.exomind.data.entities.UserObject
import com.bwael.exomind.features.main.MainActivity
import com.bwael.exomind.features.main.MainViewModel
import com.bwael.exomind.features.main.NavigationContract
import com.bwael.exomind.features.user.adapter.UsersAdapter
import kotlinx.android.synthetic.main.fragment_user_list.*
import java.util.*

class UserListFragment : BaseFragment(), UsersAdapter.UserInteraction {

    ///////////////////////////////////////////////////////////////////////////
    // VIEWMODEL SECTION
    ///////////////////////////////////////////////////////////////////////////
    lateinit var viewModel: MainViewModel

    ///////////////////////////////////////////////////////////////////////////
    // PROPERTIES SECTION
    ///////////////////////////////////////////////////////////////////////////
    private var adapterUser = UsersAdapter(this)
    private var navigationContract: NavigationContract? = null
    private val listUserSearch: MutableList<UserObject?> = mutableListOf()
    private val listUser: MutableList<UserObject?> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = viewModel()

        navigationContract = activity as MainActivity

        initObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_user_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycler()
        context?.let { ctx -> DialogUtils.showProgressDialog(ctx) }
        viewModel.retrieveUsers()
    }

    ///////////////////////////////////////////////////////////////////////////
    // INITIALISE VIEW
    ///////////////////////////////////////////////////////////////////////////
    private fun initRecycler() {
        val spanCount = if (resources.getBoolean(R.bool.isTablet)) 2 else 1
        rv_users.layoutManager = GridLayoutManager(context, spanCount)
        rv_users.adapter = adapterUser
    }

    ///////////////////////////////////////////////////////////////////////////
    // OBSERVERS INITIALISATION
    ///////////////////////////////////////////////////////////////////////////
    private fun initObservers() {
        viewModel.observeUserListLiveData().observe(this, Observer { response ->
            DialogUtils.dismissProgressDialog()
            response?.let { result ->
                if (result.status == ResourceState.SUCCESS) {
                    listUser.clear()
                    listUserSearch.clear()
                    result.data?.let { list -> adapterUser.addUsers(list) }
                    result.data?.let { list -> listUser.addAll(list) }
                    result.data?.let { list -> listUserSearch.addAll(list) }
                }
                viewModel.userListLiveData.postValue(null)
            }
        })

        viewModel.observeSearchLiveDataLiveData().observe(this, Observer { response ->
            response?.let {
                if (response.first == TAG) {
                    search(response.second)
                }
                viewModel.searchLiveData.postValue(null)
            }
        })
    }

    ///////////////////////////////////////////////////////////////////////////
    // SEARCH SECTION
    ///////////////////////////////////////////////////////////////////////////
    @SuppressLint("DefaultLocale")
    fun search(text: String) {
        listUserSearch.clear()
        if (text.isEmpty()) {
            adapterUser.addUsers(listUser)
            return
        }

        listUser.let { list ->
            for (user in list) {
                val shouldAdd = buildAllSearchedUser(text, user)
                if (shouldAdd) listUserSearch.add(user)
            }
        }

        adapterUser.addUsers(listUserSearch)
    }

    private fun buildAllSearchedUser(text: String, userObject: UserObject?): Boolean =
        userObject?.name?.contains(text) == true ||
                userObject?.username?.toLowerCase(Locale.ENGLISH)
                    ?.contains(text.toLowerCase(Locale.getDefault())) == true ||
                userObject?.email?.toLowerCase(Locale.ENGLISH)
                    ?.contains(text.toLowerCase(Locale.getDefault())) == true ||
                userObject?.phone?.toLowerCase(Locale.ENGLISH)
                    ?.contains(text.toLowerCase(Locale.getDefault())) == true ||
                userObject?.website?.toLowerCase(Locale.ENGLISH)
                    ?.contains(text.toLowerCase(Locale.getDefault())) == true

    ///////////////////////////////////////////////////////////////////////////
    // USER INTERACTION SECTION
    ///////////////////////////////////////////////////////////////////////////
    override fun retrieveUserAlbums(id: Int?) {
        navigationContract?.navigateToAlbumWithServiceLaunching(id)
    }

    override fun callUser(phone: String?) {
        val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:$phone"))
        startActivity(intent)
    }

    override fun visitWebsite(site: String) {
        try {
            val urlWww = if (!site.startsWith("www."))
                "www.$site"
            else site
            val urlHttp = if (!urlWww.startsWith("http://") && !urlWww.startsWith("https://"))
                "http://$urlWww"
            else site
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(urlHttp)))
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(
                context,
                "No application can handle this request. Please install a web browser or check your URL.",
                Toast.LENGTH_LONG
            ).show()
            e.printStackTrace()
        }
    }

    companion object {

        val TAG = UserListFragment::class.java.name

        @JvmStatic
        fun newInstance() = UserListFragment()
    }
}
