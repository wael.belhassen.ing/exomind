package com.bwael.exomind.features.user.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bwael.exomind.R
import com.bwael.exomind.data.entities.UserObject
import com.bwael.exomind.features.user.utils.UserDiffUtils
import kotlinx.android.synthetic.main.item_user_row.view.*

class UsersAdapter(private val userInteraction: UserInteraction) :
    RecyclerView.Adapter<UsersAdapter.ViewHolder>() {

    var list: MutableList<UserObject?> = mutableListOf()

    /**
     *  Pour prevoir la possibilite d'avoir une duplication de donnee,
     *  je prefere l'utilisation du diffUtils pour qu'il les geres
     **/
    fun addUsers(list: MutableList<UserObject?>) {
        val diffCallback = UserDiffUtils(this.list, list)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        this.list.clear()
        this.list.addAll(list)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_user_row, parent, false)
    )

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {
        initViewText(holder, position)
        initViewClickEvent(holder, position)
    }

    private fun initViewText(
        holder: ViewHolder,
        position: Int
    ) {
        holder.lastName.text = list[position]?.name
        holder.pseudo.text = list[position]?.username
        holder.mail.text = list[position]?.email
        holder.tel.text = list[position]?.phone
        holder.site.text = list[position]?.website
    }

    private fun initViewClickEvent(
        holder: ViewHolder,
        position: Int
    ) {
        holder.itemView.setOnClickListener {
            userInteraction.retrieveUserAlbums(list[position]?.id)
        }

        holder.tel.setOnClickListener {
            userInteraction.callUser(list[position]?.phone)
        }

        holder.site.setOnClickListener {
            userInteraction.visitWebsite(list[position]?.website ?: "www.google.com")
        }
    }

    override fun getItemCount(): Int = list.size

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val lastName: TextView = itemView.user_lastName
        val pseudo: TextView = itemView.user_pseudo
        val mail: TextView = itemView.user_mail
        val tel: TextView = itemView.user_tel
        val site: TextView = itemView.user_site
    }

    interface UserInteraction {
        fun retrieveUserAlbums(id: Int?)
        fun callUser(phone: String?)
        fun visitWebsite(site: String)
    }
}
