package com.bwael.exomind.features.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bwael.exomind.features.user.UserListViewModel
import javax.inject.Inject

class MainViewModel @Inject constructor() : UserListViewModel() {

    ///////////////////////////////////////////////////////////////////////////
    // SEARCH EXECUTE SECTION
    ///////////////////////////////////////////////////////////////////////////
    var searchLiveData: MutableLiveData<Pair<String, String>?> = MutableLiveData()
    fun observeSearchLiveDataLiveData(): LiveData<Pair<String, String>?> =
        searchLiveData
}
