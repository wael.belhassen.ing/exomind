package com.bwael.exomind.features.user.utils

import androidx.recyclerview.widget.DiffUtil
import com.bwael.exomind.data.entities.UserObject

class UserDiffUtils(
    private val oldList: MutableList<UserObject?>,
    private val newList: MutableList<UserObject?>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition]?.id == newList[newItemPosition]?.id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition]?.id == newList[newItemPosition]?.id
    }
}
