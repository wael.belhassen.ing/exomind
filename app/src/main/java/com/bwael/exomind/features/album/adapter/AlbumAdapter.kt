package com.bwael.exomind.features.album.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bwael.exomind.R
import com.bwael.exomind.data.entities.UserAlbum
import com.bwael.exomind.features.album.utils.AlbumDiffUtils
import kotlinx.android.synthetic.main.item_album_row.view.*

class AlbumAdapter(private val userInteraction: UserInteraction) :
    RecyclerView.Adapter<AlbumAdapter.ViewHolder>() {

    var list: MutableList<UserAlbum> = mutableListOf()

    /**
     *  Pour prevoir la possibilite d'avoir une duplication de donnee,
     *  je prefere l'utilisation du diffUtils pour qu'il les gere
     **/
    fun addAlbum(list: MutableList<UserAlbum>) {
        val diffCallback = AlbumDiffUtils(this.list, list)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        this.list.clear()
        this.list.addAll(list)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_album_row, parent, false)
    )

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {
        initViewText(holder, position)
        initViewClickEvent(holder, position)
    }

    private fun initViewText(
        holder: ViewHolder,
        position: Int
    ) {
        holder.lastName.text = list[position].title
    }

    private fun initViewClickEvent(
        holder: ViewHolder,
        position: Int
    ) {
        holder.itemView.setOnClickListener {
            userInteraction.retrieveUserImageAlbums(list[position].userId, list[position].id)
        }
    }

    override fun getItemCount(): Int = list.size

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val lastName: TextView = itemView.album_name
    }

    interface UserInteraction {
        fun retrieveUserImageAlbums(idUser: Int?, idAlbum: Int?)
    }
}
