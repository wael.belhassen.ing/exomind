package com.bwael.exomind.features.album.utils

import androidx.recyclerview.widget.DiffUtil
import com.bwael.exomind.data.entities.UserAlbumImage

class AlbumImageDiffUtils(
    private val oldList: MutableList<UserAlbumImage>,
    private val newList: MutableList<UserAlbumImage>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }
}
