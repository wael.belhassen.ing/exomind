package com.bwael.exomind.features.album

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bwael.exomind.common.archi.resources.Resource
import com.bwael.exomind.common.archi.resources.ResourceState
import com.bwael.exomind.common.archi.viewmodel.BaseViewModel
import com.bwael.exomind.data.entities.UserAlbum
import com.bwael.exomind.data.entities.UserAlbumImage
import com.bwael.exomind.domain.RetrieveUsersAlbums
import com.bwael.exomind.domain.RetrieveUsersAlbumsImage
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@SuppressLint("AutoDispose")
open class AlbumViewModel : BaseViewModel() {

    ///////////////////////////////////////////////////////////////////////////
    // DEPENDENCY INJECTION SECTION
    ///////////////////////////////////////////////////////////////////////////
    @Inject
    lateinit var retrieveUsersAlbums: RetrieveUsersAlbums

    @Inject
    lateinit var retrieveUsersAlbumsImage: RetrieveUsersAlbumsImage

    ///////////////////////////////////////////////////////////////////////////
    // OBSERVERS ATTRIBUTES
    ///////////////////////////////////////////////////////////////////////////
    var userAlbumsImageLiveData: MutableLiveData<Resource<MutableList<UserAlbumImage>>> =
        MutableLiveData()

    fun observeUserAlbumsImageLiveData(): LiveData<Resource<MutableList<UserAlbumImage>>> =
        userAlbumsImageLiveData

    var userAlbumsLiveData: MutableLiveData<Resource<MutableList<UserAlbum>>?> = MutableLiveData()
    fun observeUserAlbumsLiveData(): LiveData<Resource<MutableList<UserAlbum>>?> =
        userAlbumsLiveData

    ///////////////////////////////////////////////////////////////////////////
    // FUNCTION SECTION
    ///////////////////////////////////////////////////////////////////////////
    fun retrieveUsersAlbums(idUser: Int) {
        val dispose = retrieveUsersAlbumsDisposableSingle()
        retrieveUsersAlbums.execute(idUser)
            .observeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())
            .subscribe(dispose)

        disposable.add(dispose)
    }

    fun retrieveUsersAlbumsImage(idUser: Int?, idAlbum: Int?) {
        val dispose = retrieveUsersAlbumsImageDisposableSingle()
        retrieveUsersAlbumsImage.execute(Pair(idUser, idAlbum))
            .observeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())
            .subscribe(dispose)

        disposable.add(dispose)
    }

    ///////////////////////////////////////////////////////////////////////////
    // DISPOSABLES SECTION
    ///////////////////////////////////////////////////////////////////////////
    private fun retrieveUsersAlbumsDisposableSingle(): DisposableSingleObserver<MutableList<UserAlbum>> {
        return object : DisposableSingleObserver<MutableList<UserAlbum>>() {
            override fun onSuccess(t: MutableList<UserAlbum>) {
                userAlbumsLiveData.postValue(
                    Resource(
                        ResourceState.SUCCESS,
                        t,
                        null
                    )
                )
            }

            override fun onError(e: Throwable) {
                userAlbumsLiveData.postValue(
                    Resource(
                        ResourceState.ERROR,
                        null,
                        e.message
                    )
                )
            }
        }
    }

    private fun retrieveUsersAlbumsImageDisposableSingle(): DisposableSingleObserver<MutableList<UserAlbumImage>> {
        return object : DisposableSingleObserver<MutableList<UserAlbumImage>>() {
            override fun onSuccess(t: MutableList<UserAlbumImage>) {
                userAlbumsImageLiveData.postValue(
                    Resource(
                        ResourceState.SUCCESS,
                        t,
                        null
                    )
                )
            }

            override fun onError(e: Throwable) {
                userAlbumsImageLiveData.postValue(
                    Resource(
                        ResourceState.ERROR,
                        null,
                        e.message
                    )
                )
            }
        }
    }
}
