package com.bwael.exomind.features.album

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.bwael.exomind.R
import com.bwael.exomind.common.archi.fragment.BaseFragment
import com.bwael.exomind.common.archi.resources.ResourceState
import com.bwael.exomind.common.dialogs.DialogUtils
import com.bwael.exomind.common.utils.MagicNumberProvider
import com.bwael.exomind.data.entities.UserAlbumImage
import com.bwael.exomind.features.album.adapter.AlbumImageAdapter
import com.bwael.exomind.features.album.utils.AlbumItemSpacing
import com.bwael.exomind.features.main.MainActivity
import com.bwael.exomind.features.main.MainViewModel
import com.bwael.exomind.features.main.NavigationContract
import kotlinx.android.synthetic.main.fragment_album_details.*
import java.util.*

class AlbumDetailsFragment : BaseFragment() {

    ///////////////////////////////////////////////////////////////////////////
    // VIEWMODEL SECTION
    ///////////////////////////////////////////////////////////////////////////
    lateinit var viewModel: MainViewModel

    ///////////////////////////////////////////////////////////////////////////
    // PROPERTIES SECTION
    ///////////////////////////////////////////////////////////////////////////
    private var albumAdapter = AlbumImageAdapter()

    private val listUserAlbumImageSearch: MutableList<UserAlbumImage> = mutableListOf()
    private val listUserImageAlbum: MutableList<UserAlbumImage> = mutableListOf()
    private var navigationContract: NavigationContract? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = viewModel()
        initObservers()
        navigationContract = activity as MainActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_album_details, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycler()
    }

    ///////////////////////////////////////////////////////////////////////////
    // INITIALISE VIEW
    ///////////////////////////////////////////////////////////////////////////
    private fun initRecycler() {
        val spanCount =
            if (resources.getBoolean(R.bool.isTablet)) MagicNumberProvider.MAGIC_4 else 2
        rv_album_image.layoutManager = GridLayoutManager(context, spanCount)
        val spacingDecorator = resources.getDimensionPixelSize(R.dimen.spacing_small)
        rv_album_image.addItemDecoration(AlbumItemSpacing(spacingDecorator))
        rv_album_image.adapter = albumAdapter
    }

    ///////////////////////////////////////////////////////////////////////////
    // OBSERVERS SECTION
    ///////////////////////////////////////////////////////////////////////////
    private fun initObservers() {
        viewModel.observeUserAlbumsImageLiveData().observe(this, Observer { response ->
            DialogUtils.dismissProgressDialog()
            response?.let { result ->
                if (result.status == ResourceState.SUCCESS) {
                    listUserImageAlbum.clear()
                    listUserAlbumImageSearch.clear()
                    result.data?.let { list -> albumAdapter.addAlbum(list) }
                    result.data?.let { list -> listUserImageAlbum.addAll(list) }
                    result.data?.let { list -> listUserAlbumImageSearch.addAll(list) }
                }
                viewModel.userAlbumsImageLiveData.postValue(null)
            }
        })

        viewModel.observeSearchLiveDataLiveData().observe(this, Observer { response ->
            response?.let {
                if (response.first == TAG) {
                    search(response.second)
                }
                viewModel.searchLiveData.postValue(null)
            }
        })
    }

    ///////////////////////////////////////////////////////////////////////////
    // SEARCH SECTION
    ///////////////////////////////////////////////////////////////////////////
    @SuppressLint("DefaultLocale")
    fun search(text: String) {
        listUserAlbumImageSearch.clear()
        if (text.isEmpty()) {
            albumAdapter.addAlbum(listUserImageAlbum)
            return
        }

        listUserImageAlbum.let { list ->
            for (user in list) {
                val shouldAdd = buildAllSearchedAlbum(text, user)
                if (shouldAdd) listUserAlbumImageSearch.add(user)
            }
        }

        albumAdapter.addAlbum(listUserAlbumImageSearch)
    }

    private fun buildAllSearchedAlbum(text: String, userAlbum: UserAlbumImage?): Boolean =
        userAlbum?.url?.contains(text.toLowerCase(Locale.getDefault())) == true ||
                userAlbum?.thumbnailUrl?.contains(text.toLowerCase(Locale.getDefault())) == true

    companion object {

        val TAG = AlbumDetailsFragment::class.java.name

        @JvmStatic
        fun newInstance() = AlbumDetailsFragment()
    }
}
