package com.bwael.exomind.features.album.utils

import androidx.recyclerview.widget.DiffUtil
import com.bwael.exomind.data.entities.UserAlbum

class AlbumDiffUtils(
    private val oldList: MutableList<UserAlbum>,
    private val newList: MutableList<UserAlbum>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }
}
