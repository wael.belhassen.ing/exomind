package com.bwael.exomind.features.user

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bwael.exomind.common.archi.resources.Resource
import com.bwael.exomind.common.archi.resources.ResourceState
import com.bwael.exomind.data.entities.UserObject
import com.bwael.exomind.domain.RetrieveUsersUseCase
import com.bwael.exomind.features.album.AlbumViewModel
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@SuppressLint("AutoDispose")
open class UserListViewModel : AlbumViewModel() {
    ///////////////////////////////////////////////////////////////////////////
    // DEPENDENCY INJECTION SECTION
    ///////////////////////////////////////////////////////////////////////////]
    @Inject
    lateinit var retrieveUsersUseCase: RetrieveUsersUseCase

    ///////////////////////////////////////////////////////////////////////////
    // OBSERVATION ATTRIBUTES SECTION
    ///////////////////////////////////////////////////////////////////////////
    var userListLiveData: MutableLiveData<Resource<MutableList<UserObject?>?>> = MutableLiveData()
    fun observeUserListLiveData(): LiveData<Resource<MutableList<UserObject?>?>> = userListLiveData

    ///////////////////////////////////////////////////////////////////////////
    // FUNCTION SECTION
    ///////////////////////////////////////////////////////////////////////////
    fun retrieveUsers() {
        val dispose = retrieveUsersDisposableSingle()
        retrieveUsersUseCase.execute()
            .observeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())
            .subscribe(dispose)

        disposable.add(dispose)
    }

    ///////////////////////////////////////////////////////////////////////////
    // DISPOSABLES SECTION
    ///////////////////////////////////////////////////////////////////////////
    private fun retrieveUsersDisposableSingle(): DisposableSingleObserver<MutableList<UserObject?>> {
        return object : DisposableSingleObserver<MutableList<UserObject?>>() {
            override fun onSuccess(t: MutableList<UserObject?>) {
                userListLiveData.postValue(
                    Resource(
                        ResourceState.SUCCESS,
                        t,
                        null
                    )
                )
            }

            override fun onError(e: Throwable) {
                userListLiveData.postValue(
                    Resource(
                        ResourceState.ERROR,
                        null,
                        e.message
                    )
                )
            }
        }
    }
}
