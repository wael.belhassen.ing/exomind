package com.bwael.exomind.features.album

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.bwael.exomind.R
import com.bwael.exomind.common.archi.fragment.BaseFragment
import com.bwael.exomind.common.archi.resources.ResourceState
import com.bwael.exomind.common.dialogs.DialogUtils
import com.bwael.exomind.common.utils.MagicNumberProvider
import com.bwael.exomind.data.entities.UserAlbum
import com.bwael.exomind.features.album.adapter.AlbumAdapter
import com.bwael.exomind.features.album.utils.AlbumItemSpacing
import com.bwael.exomind.features.main.MainActivity
import com.bwael.exomind.features.main.MainViewModel
import com.bwael.exomind.features.main.NavigationContract
import kotlinx.android.synthetic.main.fragment_album.*
import java.util.*

class AlbumFragment : BaseFragment(), AlbumAdapter.UserInteraction {

    ///////////////////////////////////////////////////////////////////////////
    // VIEWMODEL SECTION
    ///////////////////////////////////////////////////////////////////////////
    lateinit var viewModel: MainViewModel

    ///////////////////////////////////////////////////////////////////////////
    // PROPERTIES SECTION
    ///////////////////////////////////////////////////////////////////////////
    private var albumAdapter = AlbumAdapter(this)

    private val listUserAlbumSearch: MutableList<UserAlbum> = mutableListOf()
    private val listUserAlbum: MutableList<UserAlbum> = mutableListOf()
    private var navigationContract: NavigationContract? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = viewModel()
        initObservers()

        navigationContract = activity as MainActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_album, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycler()
    }

    ///////////////////////////////////////////////////////////////////////////
    // INITIALISE VIEW
    ///////////////////////////////////////////////////////////////////////////
    private fun initRecycler() {
        val spanCount =
            if (resources.getBoolean(R.bool.isTablet)) MagicNumberProvider.MAGIC_4 else 2
        rv_album.layoutManager = GridLayoutManager(context, spanCount)
        val spacingDecorator = resources.getDimensionPixelSize(R.dimen.spacing_small)
        rv_album.addItemDecoration(AlbumItemSpacing(spacingDecorator))
        rv_album.adapter = albumAdapter
    }

    ///////////////////////////////////////////////////////////////////////////
    // OBSERVERS SECTION
    ///////////////////////////////////////////////////////////////////////////
    private fun initObservers() {
        viewModel.observeUserAlbumsLiveData().observe(this, Observer { response ->
            DialogUtils.dismissProgressDialog()
            response?.let { result ->
                if (result.status == ResourceState.SUCCESS) {
                    listUserAlbum.clear()
                    listUserAlbumSearch.clear()
                    result.data?.let { list -> albumAdapter.addAlbum(list) }
                    result.data?.let { list -> listUserAlbum.addAll(list) }
                    result.data?.let { list -> listUserAlbumSearch.addAll(list) }
                }
                viewModel.userAlbumsLiveData.postValue(null)
            }
        })

        viewModel.observeSearchLiveDataLiveData().observe(this, Observer { response ->
            response?.let {
                if (response.first == TAG) {
                    search(response.second)
                }
                viewModel.searchLiveData.postValue(null)
            }
        })
    }

    ///////////////////////////////////////////////////////////////////////////
    // SEARCH SECTION
    ///////////////////////////////////////////////////////////////////////////
    @SuppressLint("DefaultLocale")
    fun search(text: String) {
        listUserAlbumSearch.clear()
        if (text.isEmpty()) {
            albumAdapter.addAlbum(listUserAlbum)
            return
        }

        listUserAlbum.let { list ->
            for (user in list) {
                val shouldAdd = buildAllSearchedAlbum(text, user)
                if (shouldAdd) listUserAlbumSearch.add(user)
            }
        }

        albumAdapter.addAlbum(listUserAlbumSearch)
    }

    private fun buildAllSearchedAlbum(text: String, userAlbum: UserAlbum?): Boolean =
        userAlbum?.title?.contains(text.toLowerCase(Locale.getDefault())) == true

    companion object {

        val TAG = AlbumFragment::class.java.name

        @JvmStatic
        fun newInstance() = AlbumFragment()
    }

    ///////////////////////////////////////////////////////////////////////////
    // USER INTERACTION SECTION
    ///////////////////////////////////////////////////////////////////////////
    override fun retrieveUserImageAlbums(idUser: Int?, idAlbum: Int?) {
        navigationContract?.navigateToAlbumImageWithServiceLaunching(idUser, idAlbum)
    }
}
