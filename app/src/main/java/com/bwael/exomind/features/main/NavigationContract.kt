package com.bwael.exomind.features.main

interface NavigationContract {
    fun navigateToAlbumWithServiceLaunching(idUser: Int?)
    fun navigateToAlbumImageWithServiceLaunching(idUser: Int?, idAlbum: Int?)
}
