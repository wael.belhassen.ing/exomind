package com.bwael.exomind.features.album.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.bwael.exomind.R
import com.bwael.exomind.data.entities.UserAlbumImage
import com.bwael.exomind.features.album.utils.AlbumImageDiffUtils
import kotlinx.android.synthetic.main.item_album_image_row.view.*

class AlbumImageAdapter : RecyclerView.Adapter<AlbumImageAdapter.ViewHolder>() {

    var list: MutableList<UserAlbumImage> = mutableListOf()

    /**
     *  Pour prevoir la possibilite d'avoir une duplication de donnee,
     *  je prefere l'utilisation du diffUtils pour qu'il les gere
     **/
    fun addAlbum(list: MutableList<UserAlbumImage>) {
        val diffCallback = AlbumImageDiffUtils(this.list, list)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        this.list.clear()
        this.list.addAll(list)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_album_image_row, parent, false)
    )

    /**
     * For more visibilty and to avoid pixaled image, I choose to use 600x600 image for smartphone
     * and 150x150 to tablet
     **/
    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {
        val imageUrl = if (holder.img.context.resources.getBoolean(R.bool.isTablet))
            list[position].thumbnailUrl else list[position].url
        holder.img.load(imageUrl) {
            crossfade(true)
        }
    }

    override fun getItemCount(): Int = list.size

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val img: ImageView = itemView.album_image
    }
}
