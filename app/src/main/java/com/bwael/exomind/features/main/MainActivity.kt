package com.bwael.exomind.features.main

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.viewpager.widget.ViewPager
import com.bwael.exomind.R
import com.bwael.exomind.common.archi.activity.BaseActivity
import com.bwael.exomind.common.archi.fragment.BaseFragment
import com.bwael.exomind.common.archi.log.LogUtil
import com.bwael.exomind.common.dialogs.DialogUtils
import com.bwael.exomind.common.utils.ViewPagerUtils
import com.bwael.exomind.features.album.AlbumDetailsFragment
import com.bwael.exomind.features.album.AlbumFragment
import com.bwael.exomind.features.main.adapter.MainContainerAdapter
import com.bwael.exomind.features.user.UserListFragment
import com.jakewharton.rxbinding2.widget.RxTextView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar_left_logo.*

class MainActivity : BaseActivity(), NavigationContract {
    ///////////////////////////////////////////////////////////////////////////
    // BASEACTIVITY PROPERTY IMPLEMENTATION
    ///////////////////////////////////////////////////////////////////////////
    override val layoutResourceId: Int
        get() = R.layout.activity_main

    ///////////////////////////////////////////////////////////////////////////
    // VIEW MODEL
    ///////////////////////////////////////////////////////////////////////////
    private lateinit var viewModel: MainViewModel

    ///////////////////////////////////////////////////////////////////////////
    // PROPERTIES DECLARATION
    ///////////////////////////////////////////////////////////////////////////
    val listFragment by lazy { mutableListOf<BaseFragment>() }
    var mainContainerAdapter: MainContainerAdapter? = null
    var currentPosition: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)
        viewModel = viewModel()
        initViewPagerElement()
        initSearch()
        initObservers()
    }

    override fun onBackPressed() {
        if (currentPosition != 0) ViewPagerUtils.onPrevious(main_container)
        else finish()
    }

    private fun initViewPagerElement() {
        listFragment.add(UserListFragment.newInstance())
        listFragment.add(AlbumFragment.newInstance())
        listFragment.add(AlbumDetailsFragment.newInstance())

        mainContainerAdapter = MainContainerAdapter(supportFragmentManager, listFragment)

        main_container?.offscreenPageLimit = listFragment.size
        main_container?.adapter = mainContainerAdapter
        main_container?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
                LogUtil.d("onPageScrollStateChanged")
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                LogUtil.d("onPageScrolled")
            }

            override fun onPageSelected(position: Int) {
                currentPosition = position
            }
        })
    }

    ///////////////////////////////////////////////////////////////////////////
    // SEARCH USER
    ///////////////////////////////////////////////////////////////////////////
    @SuppressLint("AutoDispose")
    private fun initSearch() {
        disposable.add(
            RxTextView.textChanges(toolbar_search).doOnNext {
                val tag = when (currentPosition) {
                    0 -> UserListFragment.TAG
                    1 -> AlbumFragment.TAG
                    else -> AlbumDetailsFragment.TAG
                }
                viewModel.searchLiveData.postValue(Pair(tag, toolbar_search.text.toString()))
            }.subscribe()
        )
    }

    ///////////////////////////////////////////////////////////////////////////
    // OBSERVERS SECTION
    ///////////////////////////////////////////////////////////////////////////
    private fun initObservers() {
        viewModel.observeUserAlbumsLiveData().observe(this, Observer { response ->
            DialogUtils.dismissProgressDialog()
            response?.let {
                ViewPagerUtils.onNext(main_container)

                viewModel.userAlbumsLiveData.postValue(null)
            }
        })
        viewModel.observeUserAlbumsImageLiveData().observe(this, Observer { response ->
            DialogUtils.dismissProgressDialog()
            response?.let {
                ViewPagerUtils.onNext(main_container)

                viewModel.userAlbumsImageLiveData.postValue(null)
            }
        })
    }

    ///////////////////////////////////////////////////////////////////////////
    // NAVIGATION SECTION
    ///////////////////////////////////////////////////////////////////////////
    override fun navigateToAlbumWithServiceLaunching(idUser: Int?) {
        DialogUtils.showProgressDialog(this)
        idUser?.let { id -> viewModel.retrieveUsersAlbums(id) }
    }

    override fun navigateToAlbumImageWithServiceLaunching(idUser: Int?, idAlbum: Int?) {
        DialogUtils.showProgressDialog(this)
        viewModel.retrieveUsersAlbumsImage(idUser, idAlbum)
    }
}
