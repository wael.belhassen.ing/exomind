package com.bwael.exomind.common.archi.extension

import android.app.Activity
import android.graphics.Point
import android.view.inputmethod.InputMethodManager
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.bwael.exomind.common.archi.activity.BaseDataBindingActivity
import com.bwael.exomind.common.archi.fragment.BaseFragment

fun BaseDataBindingActivity.addFragmentSafely(
    @IdRes layout: Int,
    fragment: BaseFragment,
    tag: String,
    addToBackStack: Boolean,
    allowStateLoss: Boolean = false
) {
    if (tag.isNotEmpty()) {
        val manager = supportFragmentManager

        if (manager.findFragmentByTag(tag) == null) {
            val transaction = manager.beginTransaction()
            transaction.add(layout, fragment, tag)
            if (addToBackStack) transaction.addToBackStack(tag)
            if (!manager.isStateSaved) {
                transaction.commit()
            } else if (allowStateLoss) {
                transaction.commitAllowingStateLoss()
            }
            manager.executePendingTransactions()
        }
    }
}

fun BaseDataBindingActivity.isScreenDisplayLandscape(): Boolean {
    val window = this.window
    val size = Point()
    val display = window?.windowManager?.defaultDisplay
    display?.getSize(size)

    return size.x >= size.y
}

fun BaseDataBindingActivity.replaceFragmentSafely(
    @IdRes layout: Int,
    fragment: BaseFragment,
    tag: String,
    addToBackStack: Boolean,
    allowStateLoss: Boolean = false
) {
    if (tag.isNotEmpty()) {
        val manager = supportFragmentManager

        if (manager.findFragmentByTag(tag) == null) {
            val transaction = manager.beginTransaction()
            transaction.replace(layout, fragment, tag)
            if (addToBackStack) transaction.addToBackStack(tag)
            if (!manager.isStateSaved) {
                transaction.commit()
            } else if (allowStateLoss) {
                transaction.commitAllowingStateLoss()
            }
            manager.executePendingTransactions()
        }
    }
}

fun BaseDataBindingActivity.replaceFragmentToEmptyStack(
    @IdRes layout: Int,
    fragment: BaseFragment,
    tag: String,
    addToBackStack: Boolean,
    allowStateLoss: Boolean = false
) {
    if (tag.isNotEmpty()) {
        val manager = supportFragmentManager

        val transaction = manager.beginTransaction()
        manager.popBackStack()
        transaction.replace(layout, fragment, tag)
        if (addToBackStack) transaction.addToBackStack(tag)
        if (!manager.isStateSaved) {
            transaction.commit()
        } else if (allowStateLoss) {
            transaction.commitAllowingStateLoss()
        }
        manager.executePendingTransactions()
    }
}

fun BaseDataBindingActivity.getLastFragment(): BaseFragment? {
    val fm = supportFragmentManager
    if (fm.backStackEntryCount > 0) {
        var lastIndex = fm.backStackEntryCount - 1
        if (lastIndex < 0)
            lastIndex = 0
        val lastTag = fm.getBackStackEntryAt(lastIndex).name
        return fm.findFragmentByTag(lastTag) as BaseFragment?
    }
    return null
}

fun BaseDataBindingActivity.getFragment(obj: Class<*>): BaseFragment {
    val fragmentManager = supportFragmentManager
    return fragmentManager.findFragmentByTag(obj.name) as BaseFragment
}

fun BaseDataBindingActivity.replaceFragment(fragment: BaseFragment, frameId: Int) {
    if (!fragment.javaClass.isInstance(getLastFragment()) && isActivityRunning()) {
        val className = fragment.javaClass.name
        val fragmentManager = supportFragmentManager
        val ft = fragmentManager.beginTransaction()
        if (!fragment.isAdded) {
            ft.replace(frameId, fragment, className)
            ft.addToBackStack(className)
            ft.commit()
        } else {
            ft.show(getFragment(fragment.javaClass))
        }
    }
}

fun BaseDataBindingActivity.isActivityRunning(): Boolean = !isFinishing

fun BaseDataBindingActivity.replaceFragmentNoAddToStack(fragment: BaseFragment, frameId: Int) {
    if (!fragment.javaClass.isInstance(getLastFragment()) && isActivityRunning()) {
        val className = fragment.javaClass.name
        val fragmentManager = supportFragmentManager
        val ft = fragmentManager.beginTransaction()
        if (!fragment.isAdded) {
            ft.replace(frameId, fragment, className)
            ft.commit()
        } else {
            ft.show(getFragment(fragment.javaClass))
        }
    }
}

fun BaseDataBindingActivity.onBackClick() {
    if (canBack()) {
        val fragmentManager = supportFragmentManager
        fragmentManager.popBackStack()
    }
}

fun BaseDataBindingActivity.canBack(): Boolean = getBackStackEntryCount() > 1

fun BaseDataBindingActivity.getBackStackEntryCount(): Int {
    val fm = supportFragmentManager
    return fm.backStackEntryCount
}

fun BaseDataBindingActivity.addFragment(fragment: BaseFragment, frameId: Int) {
    if (!fragment.javaClass.isInstance(getLastFragment())) {
        val className = fragment.javaClass.name
        val fragmentManager = supportFragmentManager
        val ft = fragmentManager.beginTransaction()
        if (!fragment.isAdded) {
            ft.add(frameId, fragment, className)
            ft.addToBackStack(className)
            ft.commit()
        } else {
            ft.show(getFragment(fragment.javaClass))
        }
    }
}

fun BaseDataBindingActivity.popFragment(fragment: BaseFragment) {
    val fm = supportFragmentManager
    fm.popBackStack(fragment.javaClass.name, FragmentManager.POP_BACK_STACK_INCLUSIVE)
}

fun AppCompatActivity.hideSoftKeyboard() {
    val inputMethodManager = this.getSystemService(
        Activity.INPUT_METHOD_SERVICE
    ) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(
        this.currentFocus?.windowToken, 0
    )
}

fun AppCompatActivity.showSoftKeyboard() {
    val inputMethodManager = this.getSystemService(
        Activity.INPUT_METHOD_SERVICE
    ) as InputMethodManager
    inputMethodManager.toggleSoftInput(
        InputMethodManager.SHOW_FORCED, 0
    )
}
