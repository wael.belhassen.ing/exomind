package com.bwael.exomind.common.archi.extension

import android.app.Activity
import android.view.inputmethod.InputMethodManager
import com.bwael.exomind.common.archi.fragment.BaseFragment

fun BaseFragment.hideKeyboardFrom() {
    val hide = activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    hide.hideSoftInputFromWindow(activity?.currentFocus?.windowToken, 0)
}
