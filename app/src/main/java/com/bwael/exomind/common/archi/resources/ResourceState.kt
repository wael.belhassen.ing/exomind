package com.bwael.exomind.common.archi.resources

/**
 * Represents the state in which a [Resource] is currently in
 */
enum class ResourceState {
    LOADING, SUCCESS, ERROR
}
