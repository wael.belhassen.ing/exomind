package com.bwael.exomind.common.dialogs

import android.app.AlertDialog
import android.content.Context
import android.os.Handler
import android.view.WindowManager
import androidx.fragment.app.FragmentManager
import com.bwael.exomind.R
import com.bwael.exomind.common.dialogs.alertdialog.CustomDialog
import com.bwael.exomind.common.dialogs.alertdialog.DialogData
import com.bwael.exomind.common.dialogs.alertdialog.show

object DialogUtils {
    var dialog: AlertDialog? = null

    fun showProgressDialog(context: Context) {
        Handler().postDelayed({
            val layoutParam = WindowManager.LayoutParams()
            val builder = AlertDialog.Builder(context)
            builder.setView(R.layout.dialog_progress)
            dialog = builder.create()
            dialog?.setCancelable(false)
            dialog?.show()

            layoutParam.copyFrom(dialog?.window?.attributes)
            layoutParam.width =
                context.resources.getDimensionPixelSize(R.dimen.progress_dialog_size)
            layoutParam.height =
                context.resources.getDimensionPixelSize(R.dimen.progress_dialog_size)

            dialog?.window?.attributes = layoutParam
        }, 1)
    }

    fun dismissProgressDialog() {
        dialog?.dismiss()
    }
}

fun alertWithPersonalText(
    title: CharSequence? = null,
    message: CharSequence? = null,
    positiveButtonText: CharSequence? = null,
    fm: FragmentManager
) {

    val dialogData = DialogData()
    dialogData.title = Pair(first = title.toString(), second = !title.isNullOrEmpty())
    dialogData.description = Pair(first = message.toString(), second = !message.isNullOrEmpty())

    dialogData.positiveButtonText =
        Triple(first = positiveButtonText.toString(), second = true, third = false)

    CustomDialog().show(
        dialogData, fm
    )
}

fun alertWithPersonalTextAndEvent(
    title: CharSequence? = null,
    message: CharSequence? = null,
    positiveButtonText: CharSequence? = null,
    clickListener: () -> Unit = {},
    fm: FragmentManager
) {
    val dialogData = DialogData()
    dialogData.title = Pair(first = title.toString(), second = !title.isNullOrEmpty())
    dialogData.description = Pair(first = message.toString(), second = !message.isNullOrEmpty())

    dialogData.positiveButtonText =
        Triple(first = positiveButtonText.toString(), second = true, third = true)

    dialogData.positiveEventHandling = clickListener

    CustomDialog().show(
        dialogData, fm
    )
}

fun alertWithPersonalTextAndTwoEvent(
    textPair: Pair<String?, String?>,
    textButtonPair: Pair<CharSequence?, CharSequence?>,
    eventPair: Pair<() -> Unit, () -> Unit>,
    shouldApplyFunction: Pair<Boolean, Boolean> = Pair(first = true, second = true),
    fm: FragmentManager
) {
    val dialogData = DialogData()
    dialogData.title =
        Pair(first = textPair.first.toString(), second = !textPair.first.isNullOrEmpty())
    dialogData.description =
        Pair(first = textPair.second.toString(), second = !textPair.second.isNullOrEmpty())

    dialogData.positiveButtonText =
        Triple(
            first = textButtonPair.first.toString(),
            second = true,
            third = shouldApplyFunction.first
        )
    dialogData.negativeButtonText =
        Triple(
            first = textButtonPair.second.toString(),
            second = true,
            third = shouldApplyFunction.second
        )

    dialogData.positiveEventHandling = eventPair.first
    dialogData.negativeEventHandling = eventPair.second

    CustomDialog().show(
        dialogData, fm
    )
}

fun alertWithPersonalTextAndThreeEvent(
    textPair: Pair<String?, String?>,
    textButtonPair: Triple<CharSequence?, CharSequence?, CharSequence?>,
    eventPair: Triple<() -> Unit, () -> Unit, () -> Unit>,
    shouldApplyFunction: Triple<Boolean, Boolean, Boolean> = Triple(
        first = true,
        second = true,
        third = true
    ),
    fm: FragmentManager
) {
    val dialogData = DialogData()
    dialogData.title =
        Pair(first = textPair.first.toString(), second = !textPair.first.isNullOrEmpty())
    dialogData.description =
        Pair(first = textPair.second.toString(), second = !textPair.second.isNullOrEmpty())

    dialogData.positiveButtonText =
        Triple(
            first = textButtonPair.first.toString(),
            second = true,
            third = shouldApplyFunction.first
        )
    dialogData.negativeButtonText =
        Triple(
            first = textButtonPair.second.toString(),
            second = true,
            third = shouldApplyFunction.second
        )
    dialogData.neutralButtonText =
        Triple(
            first = textButtonPair.third.toString(),
            second = true,
            third = shouldApplyFunction.third
        )

    dialogData.positiveEventHandling = eventPair.first
    dialogData.negativeEventHandling = eventPair.second
    dialogData.neutralEventHandling = eventPair.third

    CustomDialog().show(
        dialogData, fm
    )
}

fun DialogData.builder(
    textPair: Pair<String?, String?>,
    textButtonPair: Pair<CharSequence?, CharSequence?>,
    eventPair: Pair<() -> Unit, () -> Unit>,
    shouldApplyFunction: Pair<Boolean, Boolean> = Pair(first = true, second = true)
): DialogData {

    this.title =
        Pair(first = textPair.first.toString(), second = !textPair.first.isNullOrEmpty())
    this.description =
        Pair(first = textPair.second.toString(), second = !textPair.second.isNullOrEmpty())

    this.positiveButtonText =
        Triple(
            first = textButtonPair.first.toString(),
            second = true,
            third = shouldApplyFunction.first
        )
    this.negativeButtonText =
        Triple(
            first = textButtonPair.second.toString(),
            second = true,
            third = shouldApplyFunction.second
        )

    this.positiveEventHandling = eventPair.first
    this.negativeEventHandling = eventPair.second

    return this
}
