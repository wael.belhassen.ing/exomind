package com.bwael.exomind.common.archi.activity

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bwael.exomind.common.application.App
import com.bwael.exomind.common.archi.log.LogUtil
import com.bwael.exomind.common.archi.viewmodel.event.ViewModelLifeCycleEvent
import com.bwael.exomind.common.archi.viewmodel.fatory.ViewModelFactory
import com.uber.autodispose.lifecycle.CorrespondingEventsFunction
import com.uber.autodispose.lifecycle.LifecycleEndedException
import com.uber.autodispose.lifecycle.LifecycleScopeProvider
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

/**
 * This is the base activity that centralize common functions and resources for all Activities.
 */
abstract class BaseDataBindingActivity : CommonBaseActivityBehaviour(),
    LifecycleObserver,
    LifecycleScopeProvider<ViewModelLifeCycleEvent> {

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // CONFIGURATION
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    protected abstract fun bindLayout()

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // DATA
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private val lifecycleSubject:
            BehaviorSubject<ViewModelLifeCycleEvent> = BehaviorSubject.create()

    ///////////////////////////////////////////////////////////////////////////
    // VIEWMODEL FACTORY INJECTION
    ///////////////////////////////////////////////////////////////////////////
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // LIFE CYCLE
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    override fun onCreate(savedInstanceState: Bundle?) {
        LogUtil.i(this, "onCreate BaseDataBindingActivity")
        super.onCreate(savedInstanceState)
        appComponent = (application as App).appComponentInstance()
        appComponent.inject(this)
        bindLayout()
        EventBus.getDefault().register(this)
    }

    @CallSuper
    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    ///////////////////////////////////////////////////////////////////////////
    // VIEWMODEL ACTIVITY EXTENTION
    ///////////////////////////////////////////////////////////////////////////
    inline fun <reified T : ViewModel> AppCompatActivity.viewModel(): T =
        ViewModelProvider(this, viewModelFactory)[T::class.java]

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // AUTODISPOSE
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    override fun lifecycle(): Observable<ViewModelLifeCycleEvent> = lifecycleSubject.hide()

    override fun peekLifecycle(): ViewModelLifeCycleEvent? = lifecycleSubject.value
    override fun correspondingEvents(): CorrespondingEventsFunction<ViewModelLifeCycleEvent> {
        return CorrespondingEventsFunction { testLifecycle ->
            when (testLifecycle) {
                ViewModelLifeCycleEvent.ON_CREATED -> ViewModelLifeCycleEvent.ON_CLEARED
                ViewModelLifeCycleEvent.ON_CLEARED -> throw LifecycleEndedException()
            }
        }
    }
}
