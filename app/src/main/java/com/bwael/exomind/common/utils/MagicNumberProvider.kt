package com.bwael.exomind.common.utils

object MagicNumberProvider {
    const val MAGIC_0 = 0
    const val MAGIC_1 = 1
    const val MAGIC_2 = 2
    const val MAGIC_3 = 3
    const val MAGIC_4 = 4
    const val MAGIC_5 = 5
    const val MAGIC_6 = 6
    const val MAGIC_7 = 7
    const val MAGIC_8 = 8
    const val MAGIC_9 = 9
    const val MAGIC_10 = 10
    const val MAGIC_11 = 11
    const val MAGIC_12 = 12
    const val MAGIC_13 = 13
    const val MAGIC_14 = 14
    const val MAGIC_15 = 15
    const val MAGIC_16 = 16
    const val MAGIC_17 = 17
    const val MAGIC_18 = 18
    const val MAGIC_19 = 19
    const val MAGIC_20 = 20
    const val MAGIC_21 = 21
    const val MAGIC_22 = 22
    const val MAGIC_23 = 23
    const val MAGIC_24 = 24
    const val MAGIC_25 = 25
    const val MAGIC_26 = 26
    const val MAGIC_27 = 27
    const val MAGIC_28 = 28
    const val MAGIC_29 = 29
    const val MAGIC_30 = 30
    const val MAGIC_31 = 31
    const val MAGIC_32 = 32
    const val MAGIC_33 = 33
    const val MAGIC_34 = 34
    const val MAGIC_35 = 35

    const val MAGIC_100 = 100
    const val MAGIC_200 = 200
    const val MAGIC_Lat_Kairouan = 35.6733967
    const val MAGIC_Lng_Kairouan = 10.0869376
    const val MAGIC_Lat_1 = 35.6624437
    const val MAGIC_Lng_1 = 10.0849235
    const val MAGIC_Lat_2 = 35.6624437
    const val MAGIC_Lng_2 = 10.0849235
    const val MAGIC_Lat_3 = 35.6622006
    const val MAGIC_Lng_3 = 10.0854151
}
