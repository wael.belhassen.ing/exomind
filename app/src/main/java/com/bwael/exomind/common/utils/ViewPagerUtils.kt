package com.bwael.exomind.common.utils

import androidx.viewpager.widget.ViewPager

object ViewPagerUtils {
    fun <T> getCurrent(viewPager: ViewPager, pages: List<T>): T? {
        return if (isNotEmpty(viewPager)) {
            if (viewPager.currentItem < pages.size)
                pages[viewPager.currentItem]
            else
                null
        } else {
            null
        }
    }

    fun isNotEmpty(viewPager: ViewPager): Boolean = size(viewPager) > 0

    fun size(viewPager: ViewPager?): Int {
        var count = 0
        if (viewPager != null && viewPager.adapter != null) {
            viewPager.adapter?.count?.let {
                count = it
            }
        } else {
            count = 0
        }
        return count
    }

    fun onNavigateTo(viewPager: ViewPager, index: Int) {
        if (isNotEmpty(viewPager) && index >= 0 && index < size(viewPager)) {
            viewPager.currentItem = index
        }
    }

    fun onNext(viewPager: ViewPager) {
        if (isNotEmpty(viewPager)) {
            onNavigateTo(viewPager, viewPager.currentItem + 1)
        }
    }

    fun onPrevious(viewPager: ViewPager) {
        if (isNotEmpty(viewPager)) {
            onNavigateTo(viewPager, viewPager.currentItem - 1)
        }
    }
}
