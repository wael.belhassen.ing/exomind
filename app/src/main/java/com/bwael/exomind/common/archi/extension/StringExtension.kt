package com.bwael.exomind.common.archi.extension

import android.text.TextUtils
import java.net.URLDecoder
import java.util.*

object StringExtension {

    private const val utf8 = "UTF-8"

    fun String.decodeURL(str: String): String {
        var decoded = str
        if (isNotEmpty(str)) {
            decoded = try {
                URLDecoder.decode(str, utf8)
            } catch (e: IllegalArgumentException) {
                str
            }
        }
        return decoded
    }

    fun String.upperFirstChar(str: String): String? =
        if (isNotEmpty(str)) str.substring(
            0,
            1
        ).toUpperCase(Locale.FRANCE) + str.substring(1) else str

    fun String.upperCase(str: String): String? =
        if (isNotEmpty(str)) str.toUpperCase(Locale.FRANCE) else str

    fun String.lowerCase(str: String): String? =
        if (isNotEmpty(str)) str.toLowerCase(Locale.FRANCE) else str

    fun String.isEmpty(str: CharSequence): Boolean = TextUtils.isEmpty(str)

    fun String.isNotEmpty(str: CharSequence): Boolean = !isEmpty(str)

    fun String.trim(str: String): String? =
        if (isNotEmpty(str)) str.trim { it <= ' ' }.replace("\\s+".toRegex(), "") else str

    fun String.trimAndReplace(str: String): String? =
        if (isNotEmpty(str)) trim(str)?.replace("/", "") else str

    fun String.insertChar(word: String, letter: Char, position: Int): String {
        val chars = word.toCharArray()
        val newchars = CharArray(word.length + 1)

        for (i in 0 until word.length) {
            if (i < position)
                newchars[i] = chars[i]
            else
                newchars[i + 1] = chars[i]
        }
        newchars[position] = letter
        return String(newchars)
    }

    fun String.contains(parent: String, str: String, ignoreCase: Boolean): Boolean {
        if (isNotEmpty(parent) && isNotEmpty(str))
            return parent.contains(str, ignoreCase)
        else
            return false
    }

    fun String.contains(parent: List<String>, str: String): Boolean {
        var isContained = false
        if (!isEmpty(str))
            for (i in parent.indices) {
                if (contains(parent[i], str, true))
                    isContained = true
            }
        return isContained
    }

    fun String.contains(parent: List<String>, list: List<String>): Boolean {
        for (i in list.indices) {
            if (contains(parent, list[i]))
                return true
        }
        return false
    }

    fun String.containedStringIndex(parent: List<String>, list: List<String>): Int? {
        for (i in list.indices) {
            if (contains(parent, list[i]))
                return i
        }
        return null
    }

    fun String.dotToComma(): String? {
        return if (this.contains(".")) {
            this.replace(".", ",")
        } else this
    }

    fun String.capitalize(): String {
        if (TextUtils.isEmpty(this)) {
            return this
        }
        val arr = this.toCharArray()
        var capitalizeNext = true

        val phrase = StringBuilder()
        for (c in arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c))
                capitalizeNext = false
                continue
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true
            }
            phrase.append(c)
        }

        return phrase.toString()
    }

    fun String.replaceLastOccurence(): String {
        var reverse = StringBuffer(this).reverse().toString()
        reverse = reverse.replaceFirst(".", ",")
        return StringBuffer(reverse).reverse().toString()
    }
}
