package com.bwael.exomind.common.dialogs.alertdialog

import com.bwael.exomind.common.archi.viewmodel.BaseViewModel
import javax.inject.Inject

class CustomDialogViewModel @Inject constructor() : BaseViewModel() {
    var dialogData: Pair<DialogData, Boolean> = Pair(DialogData(), false)
}
