package com.bwael.exomind.common.archi.extension

import android.content.Context
import android.graphics.Bitmap
import android.widget.Toast
import com.bwael.exomind.R
import com.bwael.exomind.data.constante.PHOTO_EXTENSION
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import kotlin.math.roundToInt

private const val TYPE_NOT_CONNECTED = 0
private const val TYPE_MOBILE = 2
private const val TYPE_WIFI = 1
private const val SAVED_IMAGE_QUALITY = 100

/** Use external media if it is available, our app's file directory otherwise */
fun Context.getOutputDirectory(): File {
    val appContext = this.applicationContext
    val mediaDir = this.externalMediaDirs.firstOrNull().let { file ->
        File(file, appContext.resources.getString(R.string.app_name)).apply { mkdirs() }
    }
    return if (mediaDir.exists())
        mediaDir else appContext.filesDir
}

fun createFile(baseFolder: File, fileName: String) =
    File(baseFolder, fileName + PHOTO_EXTENSION)

fun Context.saveBitmapImage(image: Bitmap, fileName: String): String? {
    var savedImagePath: String? = null
    val imageFileName = "$fileName.jpg"
    val storageDir = createFile(this.getOutputDirectory(), imageFileName)
    var success = true
    if (!storageDir.exists()) {
        success = storageDir.mkdirs()
    }
    if (success) {
        val imageFile = File(storageDir, imageFileName)
        savedImagePath = imageFile.absolutePath
        try {
            val fOut: OutputStream = FileOutputStream(imageFile)
            image.compress(Bitmap.CompressFormat.JPEG, SAVED_IMAGE_QUALITY, fOut)
            fOut.close()
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(this, "Error", Toast.LENGTH_LONG).show()
        }
    }
    return savedImagePath
}

fun Context.dpToPx(dp: Float): Int {
    val density = this.resources.displayMetrics.density
    return (dp * density).roundToInt()
}
