package com.bwael.exomind.common.archi.dialog

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bwael.exomind.common.application.App
import com.bwael.exomind.common.archi.viewmodel.fatory.ViewModelFactory
import javax.inject.Inject

open class BaseDialogFragment : DialogFragment() {

    ///////////////////////////////////////////////////////////////////////////
    // VIEWMODEL FACTORY INJECTION
    ///////////////////////////////////////////////////////////////////////////
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (context?.applicationContext as App).appComponentInstance().inject(this)
    }

    ///////////////////////////////////////////////////////////////////////////
    // VIEWMODEL ACTIVITY EXTENTION
    ///////////////////////////////////////////////////////////////////////////
    inline fun <reified T : ViewModel> DialogFragment.viewModel(): T =
        ViewModelProvider(this, viewModelFactory)[T::class.java]
}
