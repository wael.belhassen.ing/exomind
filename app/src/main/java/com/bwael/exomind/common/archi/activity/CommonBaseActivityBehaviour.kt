package com.bwael.exomind.common.archi.activity

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.content.res.Resources
import android.widget.ImageView
import androidx.annotation.CallSuper
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.fragment.app.FragmentTransaction
import com.bwael.exomind.common.archi.extension.hideSoftKeyboard
import com.bwael.exomind.common.archi.extension.showSoftKeyboard
import com.bwael.exomind.common.archi.fragment.BaseFragment
import com.bwael.exomind.common.archi.log.LogUtil
import com.bwael.exomind.di.component.ApplicationComponent

abstract class CommonBaseActivityBehaviour :
    AppCompatActivity() {

    ///////////////////////////////////////////////////////////////////////////
    // DI APPLICATION INSTANCE
    ///////////////////////////////////////////////////////////////////////////
    lateinit var appComponent: ApplicationComponent

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // LIFE CYCLE
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    @CallSuper
    override fun onResume() {
        LogUtil.i(this, "onResume")
        super.onResume()
    }

    @CallSuper
    override fun onPause() {
        LogUtil.i(this, "onPause")
        super.onPause()
    }

    @CallSuper
    override fun onDestroy() {
        LogUtil.i(this, "onDestroy")
        super.onDestroy()
    }

    @CallSuper
    override fun onStop() {
        LogUtil.i(this, "onStop")
        // A service can be "started" and/or "bound". In this case, it's "started" by this Activity
        // and "bound" to the JobScheduler (also called "Scheduled" by the JobScheduler). This call
        // to stopService() won't prevent scheduled jobs to be processed. However, failing
        // to call stopService() would keep it alive indefinitely.
//        stopService(Intent(this, NetworkSchedulerService::class.java))
        super.onStop()
    }

    @CallSuper
    override fun onStart() {
        LogUtil.i(this, "onStart")
        super.onStart()
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // HELPER
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    fun shouldDismissSoftKeyboard(shouldDismiss: Boolean) {
        if (shouldDismiss) hideSoftKeyboard()
        else showSoftKeyboard()
    }

    fun navigateTo(
        fragment: BaseFragment,
        @IdRes layout: Int,
        shouldAddToBackStack: Boolean = true,
        doesHaveSharedElement: Pair<Boolean, ImageView?> = Pair(false, null)
    ) {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.apply {
            initTransitionOnNavigationFor(fragmentTransaction, doesHaveSharedElement)
            if (fragment.isAdded) show(fragment)
            else replace(layout, fragment)
            addToBackStack(if (shouldAddToBackStack) fragment::class.java.name else null)
            commit()
        }
    }

    private fun initTransitionOnNavigationFor(
        manager: FragmentTransaction,
        doesHaveSharedElement: Pair<Boolean, ImageView?>
    ) {
        if (doesHaveSharedElement.first)
            doesHaveSharedElement.second?.let { img ->
                ViewCompat.getTransitionName(img)?.let { name ->
                    manager.addSharedElement(
                        img,
                        name
                    )
                }
            }
        else manager.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
    }

    @SuppressLint("SourceLockedOrientationActivity")
    @Suppress("DEPRECATION")
    override fun getResources(): Resources {
        return super.getResources().apply {
            configuration.fontScale = 1F
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            updateConfiguration(configuration, displayMetrics)
        }
    }
}
