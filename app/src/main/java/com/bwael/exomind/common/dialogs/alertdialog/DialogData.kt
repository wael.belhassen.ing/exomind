package com.bwael.exomind.common.dialogs.alertdialog

import androidx.databinding.BaseObservable
import com.bwael.exomind.common.archi.log.LogUtil

class DialogData : BaseObservable() {
    var title: Pair<String?, Boolean> = Pair(first = "", second = false)
    var description: Pair<String?, Boolean> =
        Pair(first = "", second = false)
    var positiveButtonText: Triple<String?, Boolean, Boolean> =
        Triple(first = "", second = false, third = false)
    var negativeButtonText: Triple<String?, Boolean, Boolean> =
        Triple(first = "", second = false, third = false)
    var neutralButtonText: Triple<String?, Boolean, Boolean> =
        Triple(first = "", second = false, third = false)

    var positiveEventHandling: () -> Unit = { LogUtil.d("positionEventHandling") }
    var neutralEventHandling: () -> Unit = { LogUtil.d("neutralEventHandling") }
    var negativeEventHandling: () -> Unit = { LogUtil.d("negativeEventHandling") }

    override fun toString(): String {
        return "DialogData(" +
                "\ntitle=$title," +
                "\ndescription=$description," +
                "\npositiveButtonText=$positiveButtonText," +
                "\nnegativeButtonText=$negativeButtonText," +
                "\nneutralButtonText=$neutralButtonText" +
                "\n)"
    }
}
