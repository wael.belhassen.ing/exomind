package com.bwael.exomind.common.archi.extension

import android.graphics.Point
import androidx.fragment.app.DialogFragment
import com.bwael.exomind.common.archi.fragment.BaseFragment

fun DialogFragment.isScreenDisplayLandscape(): Boolean {
    val window = this.dialog?.window
    val size = Point()
    val display = window?.windowManager?.defaultDisplay
    display?.getSize(size)

    return size.x >= size.y
}

fun BaseFragment.isScreenDisplayLandscape(): Boolean {
    val window = this.activity?.window
    val size = Point()
    val display = window?.windowManager?.defaultDisplay
    display?.getSize(size)

    return size.x >= size.y
}
