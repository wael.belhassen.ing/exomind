@file:Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")

package com.bwael.exomind.common.utils

import com.bwael.exomind.common.archi.log.LogUtil
import java.text.SimpleDateFormat
import java.util.*
import java.util.Calendar.*

object DateUtils : DateUtilsExtended() {

    fun convertStrToDateFormateOne(strDate: String): String? {
        var dateConverted: String? = null
        if (strDate.isNotEmpty()) {
            try {
                val simpleDateFormat = SimpleDateFormat(DATE_PATTERN_ONE, Locale.getDefault())
                val simpleDateFormat1 = SimpleDateFormat("ddMMyy", Locale.getDefault())
                val date1 = simpleDateFormat.parse(strDate)
                dateConverted = simpleDateFormat1.format(date1)
            } catch (ex: Exception) {
                LogUtil.i("convertion", "failed")
            }
        }
        return dateConverted
    }

    fun convertStrToDateFormatetwo(strDate: String): String? {
        var date: String? = null
        if (strDate.isNotEmpty()) {
            try {
                val simpleDateFormat1 = SimpleDateFormat(DATE_PATTERN_ONE, Locale.getDefault())
                val simpleDateFormat2 = SimpleDateFormat(DATE_PATTERN_TWO, Locale.getDefault())
                val date1 = simpleDateFormat1.parse(strDate)
                date = simpleDateFormat2.format(date1)
            } catch (ex: Exception) {
                LogUtil.i("convertion", "failed")
            }
        }
        return date
    }

    fun convertStrToDateFormateThree(strDate: String): String? {
        var date: String? = null
        if (strDate.isNotEmpty()) {
            try {
                val simpleDateFormat1 = SimpleDateFormat(DATE_PATTERN_ONE, Locale.getDefault())
                val simpleDateFormat2 = SimpleDateFormat(DATE_PATTERN_THREE, Locale.getDefault())
                val date1 = simpleDateFormat1.parse(strDate)
                date = simpleDateFormat2.format(date1)
            } catch (ex: Exception) {
                LogUtil.i("convertion", "failed")
            }
        }
        return date
    }

    fun convertStrToDateFormatetwoReversed(strDate: String): String? {
        var date: String? = null
        if (strDate.isNotEmpty()) {
            try {
                val simpleDateFormat1 = SimpleDateFormat(DATE_PATTERN_TWO, Locale.getDefault())
                val simpleDateFormat2 = SimpleDateFormat(DATE_PATTERN_ONE, Locale.getDefault())
                val date1 = simpleDateFormat1.parse(strDate)
                date = simpleDateFormat2.format(date1)
            } catch (ex: Exception) {
                LogUtil.i("convertion", "failed")
            }
        }
        return date
    }

    fun convertStrToDate(strDate: String): Date? {
        var date: Date? = null
        if (strDate.isNotEmpty()) {
            try {
                val simpleDateFormat1 = SimpleDateFormat(DATE_PATTERN_ONE, Locale.getDefault())
                date = simpleDateFormat1.parse(strDate)
            } catch (ex: Exception) {
                LogUtil.i("convertion", "failed")
            }
        }
        return date
    }

    fun convertStrToDateChat(strDate: String): String? {
        var date: String? = null
        if (strDate.isNotEmpty()) {
            try {
                val simpleDateFormat1 =
                    SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
                val simpleDateFormat2 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
                val simpleDateFormat4 = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
                val date1 = simpleDateFormat1.parse(strDate)
                val simpleDateFormat3 = SimpleDateFormat(DATE_PATTERN_TWO, Locale.getDefault())
                val c = getInstance()
                c.time = Date()
                val t = simpleDateFormat3.format(c.time)
                date = if (simpleDateFormat3.format(date1) == t)
                    simpleDateFormat4.format(date1)
                else
                    simpleDateFormat2.format(date1)
            } catch (ex: Exception) {
                LogUtil.i("convertion", "failed")
            }
        }
        return date
    }

    fun getYearFromTS(date: Date): Int {
        val cal = getInstance()
        cal.time = date
        return cal.get(YEAR)
    }

    fun getMonthFromTS(date: Date): Int {
        val cal = getInstance()
        cal.time = date
        return cal.get(MONTH) + 1
    }

    fun getMonthTS(date: Date): Int {
        val cal = getInstance()
        cal.time = date
        return cal.get(MONTH)
    }

    fun getDaysFromTS(date: Date): Int {
        val cal = getInstance()
        cal.time = date
        return cal.get(DAY_OF_MONTH)
    }

    fun getTodayDateAsString(): String {
        val date = GregorianCalendar()
        return SimpleDateFormat(DATE_PATTERN_ONE, Locale.getDefault()).format(date.time)
    }

    fun convertDateToString(date: Date): String =
        SimpleDateFormat(DATE_PATTERN_ONE, Locale.getDefault()).format(date.time)

    fun getCalendarForNow(): Calendar {
        val calendar = getInstance()
        calendar.time = Date()
        return calendar
    }

    fun getDate(date: Date): Pair<Date, Date> {
        val month = getMonthTS(date)
        val aCalendar = getInstance()
        aCalendar.time = date
        aCalendar.set(MONTH, month)
        aCalendar.set(DATE, 1)
        val firstDate = aCalendar.time
        aCalendar.set(DATE, aCalendar.getActualMaximum(DAY_OF_MONTH))
        val lastDate = aCalendar.time
        return Pair(firstDate, lastDate)
    }

    fun getDateRange(): ArrayList<Pair<String, Date>> {
        val dates = ArrayList<Pair<String, Date>>()
        val calendar: Calendar = getCalendarForNow()
        val calendar2: Calendar = getCalendarForNow()
        calendar2.add(YEAR, -1)
        while (calendar2 <= calendar) {
            val year = calendar2.get(YEAR)
            if (year < calendar.get(YEAR)) {
                val dte = SimpleDateFormat(
                    "MMMM",
                    Locale.getDefault()
                ).format(calendar2.time) + " " + year
                dates.add(Pair(dte, calendar2.time))
            } else {
                dates.add(
                    Pair(
                        SimpleDateFormat("MMMM", Locale.getDefault()).format(calendar2.time),
                        calendar2.time
                    )
                )
            }
            calendar2.add(MONTH, 1)
        }

        val dte = SimpleDateFormat("MMMM", Locale.getDefault()).format(getCalendarForNow().time)
        if (!dates.contains(Pair(dte, dates[dates.size - 1].second)))
            dates.add(Pair(dte, getCalendarForNow().time))
        return dates
    }
}
