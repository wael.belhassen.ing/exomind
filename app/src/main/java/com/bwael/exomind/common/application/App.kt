package com.bwael.exomind.common.application

import androidx.multidex.MultiDexApplication
import com.bwael.exomind.BuildConfig
import com.bwael.exomind.di.component.ApplicationComponent
import com.bwael.exomind.di.component.DaggerApplicationComponent
import timber.log.Timber

class App : MultiDexApplication() {

    private val appComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.factory().create(applicationContext)
    }

    fun appComponentInstance() = appComponent

    ///////////////////////////////////////////////////////////////////////////
    // APPLICATION LIFECYCLE IMPLEMENTATION
    ///////////////////////////////////////////////////////////////////////////
    override fun onCreate() {
        super.onCreate()
        setupTimber()
        setupLogging()
    }

    ///////////////////////////////////////////////////////////////////////////
    // APPLICATION CONFIGURATION IMPLEMENTATION
    ///////////////////////////////////////////////////////////////////////////
    private fun setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun setupLogging() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}
