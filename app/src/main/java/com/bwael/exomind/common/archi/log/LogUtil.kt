@file:Suppress("NOTHING_TO_INLINE")

package com.bwael.exomind.common.archi.log

import com.bwael.exomind.common.archi.usecase.BaseUseCase
import io.reactivex.Observable
import timber.log.Timber

object LogUtil {
    inline fun d(context: Any, msg: String) {
        Timber.tag("$context == Exomind").d(msg)
    }

    inline fun d(msg: String) {
        Timber.d(msg)
    }

    inline fun i(context: Any, msg: String) {
        Timber.tag("$context == Exomind").i(msg)
    }

    inline fun i(tag: String, msg: String) {
        Timber.tag("$tag == Exomind").i(msg)
    }

    fun iuc(useCase: BaseUseCase, parameter: Any?) {
        Timber.tag("$useCase == Exomind").i("execution with $parameter")
    }

    fun e(context: Any, msg: String) {
        Timber.tag("$context == Exomind").e(msg)
    }

    fun e(msg: String) {
        Timber.tag("Exomind").e(msg)
    }
}

inline fun <T> Observable<T>.logOnNext(tag: String): Observable<T> =
    this.doOnNext { Timber.tag("$tag:_Exomind").v("onNext with : $it") }

inline fun <T> Observable<T>.logOnSubscribe(tag: String): Observable<T> =
    this.doOnSubscribe { Timber.tag("$tag:_Exomind").i("onSubscribe") }

inline fun <T> Observable<T>.logOnComplete(tag: String): Observable<T> =
    this.doOnComplete { Timber.tag("$tag:_Exomind").i("onComplete") }

inline fun <T> Observable<T>.logOnDispose(tag: String): Observable<T> =
    this.doOnDispose { Timber.tag("$tag:_Exomind").i("onDispose") }

inline fun <T> Observable<T>.logOnTerminate(tag: String): Observable<T> =
    this.doOnTerminate { Timber.tag("$tag:_Exomind").i("onTerminate") }

inline fun <T> Observable<T>.logLifeCycle(tag: String): Observable<T> =
    this.logOnSubscribe("$tag: Exomind")
        .logOnComplete("$tag: Exomind")
        .logOnDispose("$tag: Exomind")
        .logOnTerminate("$tag: Exomind")
