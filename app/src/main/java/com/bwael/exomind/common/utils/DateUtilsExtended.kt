package com.bwael.exomind.common.utils

import android.text.format.DateUtils
import com.bwael.exomind.common.archi.log.LogUtil
import java.text.SimpleDateFormat
import java.util.*

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS",
    "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS"
)
open class DateUtilsExtended {
    companion object {
        const val SUBTRACTED_VALUE_NEGATIVE_2 = -2
        const val DATE_PATTERN_ONE = "yyyy-MM-dd"
        const val DATE_PATTERN_ONE_REVERSE = "dd-MM-yyyy"
        const val DATE_PATTERN_TWO = DATE_PATTERN_ONE
        const val DATE_PATTERN_THREE = "dd/MM/yyyy"
    }

    fun hourCountToDate(date: String?): Int {
        try {
            val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS", Locale.getDefault())
            val parsedDate = dateFormat.parse(date?.replace("T", " "))
            val timestamp = parsedDate.time
            val now = Date().time
            val ellapsedTime =
                SimpleDateFormat("hh:mm:ss", Locale.getDefault()).format(now - timestamp)
            @Suppress(
                "DEPRECATION",
                "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS"
            ) return SimpleDateFormat(
                "hh:mm:ss",
                Locale.getDefault()
            ).parse(ellapsedTime).hours
        } catch (e: Exception) { //this generic but you can control another types of exception
            return -1
        }
    }

    fun convertTimeStampToStringDate(date: String?): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS", Locale.getDefault())
        val parsedDate = dateFormat.parse(date?.replace("T", " "))
        return SimpleDateFormat(DATE_PATTERN_TWO, Locale.getDefault()).format(parsedDate.time)
    }

    fun convertStrToDateFormatethree(strDate: String): String? {
        var date: String? = null
        if (strDate.isNotEmpty()) {
            try {
                val simpleDateFormat1 = SimpleDateFormat(DATE_PATTERN_ONE, Locale.FRANCE)
                val simpleDateFormat2 = SimpleDateFormat(DATE_PATTERN_ONE_REVERSE, Locale.FRANCE)
                val date1 = simpleDateFormat1.parse(strDate)
                date = simpleDateFormat2.format(date1)
            } catch (ex: Exception) {
                LogUtil.i("convertion", "failed")
            }
        }
        return date
    }

    fun convertStrToDateWithSlash(strDate: String): Date? {
        var date: Date? = null
        if (strDate.isNotEmpty()) {
            try {
                val simpleDateFormat1 = SimpleDateFormat(DATE_PATTERN_TWO, Locale.FRANCE)
                date = simpleDateFormat1.parse(strDate)
            } catch (ex: Exception) {
                LogUtil.i("convertion", "failed")
            }
        }
        return date
    }

    fun getDateFromTimeStamp(timestamp: Long): Date = Date(timestamp * DateUtils.SECOND_IN_MILLIS)

    fun getTrimesters(): ArrayList<Pair<String, Date>> {
        val dates = ArrayList<Pair<String, Date>>()
        val calendar: Calendar = com.bwael.exomind.common.utils.DateUtils.getCalendarForNow()
        val calendar2: Calendar = com.bwael.exomind.common.utils.DateUtils.getCalendarForNow()
        calendar2.add(Calendar.YEAR, -1)
        while (calendar.after(calendar2)) {
            val year = calendar.get(Calendar.YEAR)
            if (year <= calendar2.get(Calendar.YEAR)) {
                val dateEnd = SimpleDateFormat("MMMM", Locale.getDefault()).format(calendar.time)
                val date = calendar.time
                calendar.add(Calendar.MONTH, SUBTRACTED_VALUE_NEGATIVE_2)
                val dateStart = SimpleDateFormat("MMMM", Locale.getDefault()).format(calendar.time)
                val dte = "$dateStart-$dateEnd $year"
                dates.add(Pair(dte, date))
                calendar.add(Calendar.MONTH, -1)
            } else {
                val dateEnd = SimpleDateFormat("MMMM", Locale.getDefault()).format(calendar.time)
                val date = calendar.time
                calendar.add(Calendar.MONTH, SUBTRACTED_VALUE_NEGATIVE_2)
                val dateStart = SimpleDateFormat("MMMM", Locale.getDefault()).format(calendar.time)
                val dte = "$dateStart-$dateEnd"
                dates.add(Pair(dte, date))
                calendar.add(Calendar.MONTH, -1)
            }
        }
        val year = calendar.get(Calendar.YEAR)
        val dateEnd = SimpleDateFormat("MMMM", Locale.getDefault()).format(calendar.time)
        val date = calendar.time
        calendar.add(Calendar.MONTH, SUBTRACTED_VALUE_NEGATIVE_2)
        val dateStart = SimpleDateFormat("MMMM", Locale.getDefault()).format(calendar.time)
        val dte = "$dateStart-$dateEnd $year"
        dates.add(Pair(dte, date))
        calendar.add(Calendar.MONTH, -1)
        return dates.reversed() as ArrayList<Pair<String, Date>>
    }
}
