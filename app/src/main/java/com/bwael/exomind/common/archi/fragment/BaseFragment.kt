package com.bwael.exomind.common.archi.fragment

import android.app.Activity
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bwael.exomind.common.application.App
import com.bwael.exomind.common.archi.extension.hideKeyboardFrom
import com.bwael.exomind.common.archi.viewmodel.fatory.ViewModelFactory
import com.bwael.exomind.di.component.ApplicationComponent
import javax.inject.Inject

abstract class BaseFragment : Fragment() {
    ///////////////////////////////////////////////////////////////////////////
    // DI APPLICATION INSTANCE
    ///////////////////////////////////////////////////////////////////////////

    val appComponent: ApplicationComponent? by lazy {
        // Creates an instance of AppComponent using its Factory constructor
        // We pass the applicationContext that will be used as Context in the graph
        (activity?.application as App?)?.appComponentInstance()
    }

    ///////////////////////////////////////////////////////////////////////////
    // VIEWMODEL FACTORY INJECTION
    ///////////////////////////////////////////////////////////////////////////
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // LIFE CYCLE
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent?.inject(this)
    }

    fun hideKey() {
        hideKeyboardFrom()
    }

    ///////////////////////////////////////////////////////////////////////////
    // VIEWMODEL FRAGMENT EXTENTION
    ///////////////////////////////////////////////////////////////////////////
    /**
     * This is viewmodel extension for fragment needed viewmodel provider.
     */
    inline fun <reified T : ViewModel> Fragment.viewModel(): T =
        ViewModelProvider(this, viewModelFactory)[T::class.java]

    fun hideKeyboard() {
        val inputMethodManager = activity?.getSystemService(
            Activity.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(
            activity?.currentFocus?.windowToken, 0
        )
    }
}
