package com.bwael.exomind.common.archi.extension

import com.bwael.exomind.common.archi.fragment.BaseFragment
import com.bwael.exomind.common.utils.ModeScreen

fun Int.displayModeScreen(fragment: BaseFragment): ModeScreen {

    val isLandscape = fragment.isScreenDisplayLandscape()

    return fragment.context?.resources?.getBoolean(this)?.let { isTablet ->
        when {
            isLandscape && isTablet -> ModeScreen.TABLET_LANDSCAPE
            !isLandscape && isTablet -> ModeScreen.TABLET_PORTRAIT
            isLandscape && !isTablet -> ModeScreen.PHONE_LANDSCAPE
            else -> ModeScreen.PHONE_PORTRAIT
        }
    } ?: ModeScreen.PHONE_PORTRAIT
}
