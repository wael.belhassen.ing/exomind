package com.bwael.exomind.common.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.NonNull
import androidx.constraintlayout.widget.ConstraintLayout
import com.bwael.exomind.R
import kotlinx.android.synthetic.main.toolbar.view.*

class Toolbar : ConstraintLayout, View.OnClickListener {

    ///////////////////////////////////////////////////////////////////////////
    // UI REFERENCES
    ///////////////////////////////////////////////////////////////////////////
    var toolbarInteractionInterface: ToolbarInteractionInterface? = null

    constructor(@NonNull context: Context) : super(context)

    constructor(@NonNull context: Context, @NonNull attr: AttributeSet) : super(context, attr) {
        LayoutInflater.from(context).inflate(R.layout.toolbar, this)
        toolbar_previous.setOnClickListener(this)
        toolbar_basket.setOnClickListener(this)
        toolbar_notification.setOnClickListener(this)
    }

    ///////////////////////////////////////////////////////////////////////////
    // CLICK HANDLING
    ///////////////////////////////////////////////////////////////////////////
    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.toolbar_previous -> toolbarInteractionInterface?.clickPreviousHandling()
            R.id.toolbar_basket -> toolbarInteractionInterface?.clickBasketHandling()
            R.id.toolbar_notification -> toolbarInteractionInterface?.clickNotificationHandling()
        }
    }

    fun shouldDisplayLogoUi(shouldDisplay: Boolean) {
        if (shouldDisplay) {
            toolbar_title?.visibility = View.GONE
            toolbar_logo?.visibility = View.VISIBLE
        } else {
            toolbar_title?.visibility = View.VISIBLE
            toolbar_logo?.visibility = View.GONE
        }
    }

    fun setTitle(title: String) {
        toolbar_title?.text = title
    }

    fun shouldHideBack(shouldHide: Boolean = true) {
        toolbar_previous.visibility = if (shouldHide) View.GONE else View.VISIBLE
    }

    interface ToolbarInteractionInterface {
        fun clickPreviousHandling()
        fun clickBasketHandling()
        fun clickNotificationHandling()
    }
}
