package com.bwael.exomind.common.archi.activity.provider

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import javax.inject.Provider

/**
 * This class is made to centralize lifecycle owner's life cycle.
 */
class LifecycleRegistryProvider(var owner: LifecycleOwner) :
    Provider<LifecycleRegistry> {
    override fun get(): LifecycleRegistry = LifecycleRegistry(owner)
}
