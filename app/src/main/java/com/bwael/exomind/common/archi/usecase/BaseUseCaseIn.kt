package com.bwael.exomind.common.archi.usecase

/**
 * Represents the use case behaviour which only get to have and input without output.
 */
abstract class BaseUseCaseIn<in IN> : BaseUseCase() {
    abstract fun execute(input: IN)
}
