package com.bwael.exomind.common.archi.usecase

/**
 * Represents the use case behaviour which have input and output.
 */
abstract class BaseUseCaseInOut<in IN, out OUT> : BaseUseCase() {
    abstract fun execute(parameter: IN): OUT
}
