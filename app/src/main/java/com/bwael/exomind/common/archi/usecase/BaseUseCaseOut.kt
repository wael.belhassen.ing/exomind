package com.bwael.exomind.common.archi.usecase

/**
 * Represents the use case behaviour which only get to return data without input.
 */
abstract class BaseUseCaseOut<out OUT> : BaseUseCase() {
    abstract fun execute(): OUT
}
