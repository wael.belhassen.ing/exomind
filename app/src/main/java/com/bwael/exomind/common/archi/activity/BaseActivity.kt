package com.bwael.exomind.common.archi.activity

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bwael.exomind.common.application.App
import com.bwael.exomind.common.archi.log.LogUtil
import com.bwael.exomind.common.archi.viewmodel.event.ViewModelLifeCycleEvent
import com.bwael.exomind.common.archi.viewmodel.fatory.ViewModelFactory
import com.uber.autodispose.lifecycle.CorrespondingEventsFunction
import com.uber.autodispose.lifecycle.LifecycleEndedException
import com.uber.autodispose.lifecycle.LifecycleScopeProvider
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

abstract class BaseActivity : CommonBaseActivityBehaviour(),
    LifecycleObserver,
    LifecycleScopeProvider<ViewModelLifeCycleEvent> {

    var disposable: CompositeDisposable = CompositeDisposable()

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // CONFIGURATION
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    @get:LayoutRes
    protected abstract val layoutResourceId: Int

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // DATA
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private val lifecycleSubject:
            BehaviorSubject<ViewModelLifeCycleEvent> = BehaviorSubject.create()

    ///////////////////////////////////////////////////////////////////////////
    // VIEWMODEL FACTORY INJECTION
    ///////////////////////////////////////////////////////////////////////////
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // LIFE CYCLE
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    override fun onCreate(savedInstanceState: Bundle?) {
        LogUtil.i(this, "onCreate BaseActivity")
        super.onCreate(savedInstanceState)
        setContentView(layoutResourceId)
        appComponent = (application as App).appComponentInstance()
        appComponent.inject(this)
    }

    @CallSuper
    override fun onDestroy() {
        LogUtil.i(this, "onDestroy BaseActivity")
        disposable.dispose()
        super.onDestroy()
    }

    ///////////////////////////////////////////////////////////////////////////
    // VIEWMODEL ACTIVITY EXTENTION
    ///////////////////////////////////////////////////////////////////////////
    inline fun <reified T : ViewModel> AppCompatActivity.viewModel(): T =
        ViewModelProvider(this, viewModelFactory)[T::class.java]

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // AUTODISPOSE
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    override fun lifecycle(): Observable<ViewModelLifeCycleEvent> = lifecycleSubject.hide()

    override fun peekLifecycle(): ViewModelLifeCycleEvent? = lifecycleSubject.value
    override fun correspondingEvents(): CorrespondingEventsFunction<ViewModelLifeCycleEvent> {
        return CorrespondingEventsFunction { testLifecycle ->
            when (testLifecycle) {
                ViewModelLifeCycleEvent.ON_CREATED -> ViewModelLifeCycleEvent.ON_CLEARED
                ViewModelLifeCycleEvent.ON_CLEARED -> throw LifecycleEndedException()
            }
        }
    }
}
