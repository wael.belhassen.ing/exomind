package com.bwael.exomind.common.utils

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.VectorDrawable
import android.os.Build
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
private fun VectorDrawable.getBitmap(): Bitmap {
    val bitmap = Bitmap.createBitmap(
        this.intrinsicWidth,
        this.intrinsicHeight, Bitmap.Config.ARGB_8888
    )
    val canvas = Canvas(bitmap)
    this.setBounds(0, 0, canvas.width, canvas.height)
    this.draw(canvas)
    return bitmap
}

private fun VectorDrawableCompat.getBitmap(): Bitmap {
    val bitmap = Bitmap.createBitmap(
        this.intrinsicWidth,
        this.intrinsicHeight, Bitmap.Config.ARGB_8888
    )
    val canvas = Canvas(bitmap)
    this.setBounds(0, 0, canvas.width, canvas.height)
    this.draw(canvas)
    return bitmap
}

fun Context.getBitmap(@DrawableRes drawableResId: Int): Bitmap {
    return when (val drawable = ContextCompat.getDrawable(this, drawableResId)) {
        is BitmapDrawable -> drawable.bitmap
        is VectorDrawableCompat -> drawable.getBitmap()
        is VectorDrawable -> drawable.getBitmap()
        else -> throw IllegalArgumentException("Unsupported drawable type")
    }
}
