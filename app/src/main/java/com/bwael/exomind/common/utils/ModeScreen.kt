package com.bwael.exomind.common.utils

enum class ModeScreen {
    TABLET_PORTRAIT,
    TABLET_LANDSCAPE,
    PHONE_PORTRAIT,
    PHONE_LANDSCAPE
}
