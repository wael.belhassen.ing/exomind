package com.bwael.exomind.common.utils

import android.graphics.Point
import android.view.Gravity
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import com.bwael.exomind.common.archi.extension.isScreenDisplayLandscape

object FragmentUtils {
    fun setHeightSize(fragment: DialogFragment, height: Double, gravity: Int) {
        val window = fragment.dialog?.window
        val size = Point()
        val display = window?.windowManager?.defaultDisplay
        display?.getSize(size)
        window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, (size.x * height).toInt())
        window?.setGravity(gravity)
    }

    fun setWindowFullSize(fragment: DialogFragment) {
        val window = fragment.dialog?.window
        window?.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        window?.setGravity(Gravity.CENTER)
    }

    fun setDialogSize(fragment: DialogFragment, gravity: Int) {
        val window = fragment.dialog?.window
        val size = Point()
        val display = window?.windowManager?.defaultDisplay
        display?.getSize(size)
        window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        window?.setGravity(gravity)
    }

    fun setWindowFullSizeSearch(fragment: DialogFragment) {
        val window = fragment.dialog?.window
        window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        window?.setGravity(Gravity.CENTER)
    }

    fun setDialogSize(fragment: DialogFragment, ratio: Double = 0.0, gravity: Int) {
        val window = fragment.dialog?.window
        val size = Point()
        val display = window?.windowManager?.defaultDisplay
        display?.getSize(size)
        if (fragment.isScreenDisplayLandscape())
            window?.setLayout(
                (size.x * ratio).toInt(),
                WindowManager.LayoutParams.WRAP_CONTENT
            )
        else window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        window?.setGravity(gravity)
    }
}
