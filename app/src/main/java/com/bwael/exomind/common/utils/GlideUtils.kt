package com.bwael.exomind.common.utils

import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.bwael.exomind.R

object GlideUtils {
    val requestOptions: RequestOptions by lazy {
        RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.NONE).override(
                Target.SIZE_ORIGINAL
            )
            .placeholder(R.mipmap.ic_launcher_round)
            .skipMemoryCache(true)
            .centerInside()
    }
}
