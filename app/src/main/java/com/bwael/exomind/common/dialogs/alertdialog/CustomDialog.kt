package com.bwael.exomind.common.dialogs.alertdialog

import android.app.Dialog
import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import com.bwael.exomind.BR
import com.bwael.exomind.R
import com.bwael.exomind.common.archi.dialog.BaseDialogFragment
import com.bwael.exomind.common.utils.FragmentUtils
import com.bwael.exomind.databinding.DialogCustomViewBinding

fun CustomDialog.show(dialogData: DialogData, fm: FragmentManager) {
    this.isCancelable = false
    this.newInstance(dialogData).show(fm, CustomDialog::class.java.name)
}

class CustomDialog : BaseDialogFragment() {

    private lateinit var viewModel: CustomDialogViewModel

    private lateinit var binding: DialogCustomViewBinding
    var dialogData: DialogData = DialogData()

    fun newInstance(dialogData: DialogData): CustomDialog {
        val dialog = CustomDialog()
        dialog.dialogData = dialogData
        dialog.isCancelable = false
        return dialog
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        viewModel = viewModel()
        if (!viewModel.dialogData.second) viewModel.dialogData = Pair(dialogData, true)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        return super.onCreateDialog(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.dialog_custom_view,
            container,
            false
        )

        binding.lifecycleOwner = this
        binding.apply {
            initClickListener()
            this.setVariable(BR.dialogFragmentBindingClass, viewModel)
            invalidateAll()
        }

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        FragmentUtils.setDialogSize(fragment = this, ratio = 0.5, gravity = Gravity.CENTER)
    }

    ///////////////////////////////////////////////////////////////////////////
    // CLICK EVENT HANDLING
    ///////////////////////////////////////////////////////////////////////////
    private fun initClickListener() {
        initDataEventIfNotSet()
        binding.positiveBtn.setOnClickListener {
            dialogData.positiveEventHandling()
        }
        binding.negativeBtn.setOnClickListener {
            dialogData.negativeEventHandling()
        }
        binding.neutralBtn.setOnClickListener {
            dialogData.neutralEventHandling()
        }
    }

    private fun initDataEventIfNotSet() {
        if (!dialogData.positiveButtonText.third) dialogData.positiveEventHandling =
            { dismiss() }
        if (!dialogData.negativeButtonText.third) dialogData.negativeEventHandling =
            { dismiss() }
        if (!dialogData.neutralButtonText.third) dialogData.neutralEventHandling =
            { dismiss() }
    }
}
