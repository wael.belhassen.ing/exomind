package com.bwael.exomind.common.utils

import android.content.Context
import android.view.animation.Interpolator
import android.widget.Scroller

class FixedSpeedScroller : Scroller {
    private var mDuration = DURATION

    companion object {
        const val DURATION = 500
    }

    constructor(context: Context?) : super(context)
    constructor(
        context: Context?,
        interpolator: Interpolator?
    ) : super(context, interpolator)

    constructor(
        context: Context?,
        interpolator: Interpolator?,
        flywheel: Boolean
    ) : super(context, interpolator, flywheel)

    override fun startScroll(
        startX: Int,
        startY: Int,
        dx: Int,
        dy: Int,
        duration: Int
    ) {
        super.startScroll(startX, startY, dx, dy, mDuration)
    }

    override fun startScroll(
        startX: Int,
        startY: Int,
        dx: Int,
        dy: Int
    ) {
        super.startScroll(startX, startY, dx, dy, mDuration)
    }

    fun setScrollDuration(duration: Int) {
        mDuration = duration
    }
}
