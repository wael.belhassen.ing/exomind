package com.bwael.exomind.common.archi.viewmodel

import androidx.annotation.CallSuper
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel
import com.bwael.exomind.common.archi.viewmodel.event.ViewModelLifeCycleEvent
import com.uber.autodispose.lifecycle.CorrespondingEventsFunction
import com.uber.autodispose.lifecycle.LifecycleEndedException
import com.uber.autodispose.lifecycle.LifecycleScopeProvider
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber

abstract class BaseViewModel :
    LifecycleObserver,
    LifecycleScopeProvider<ViewModelLifeCycleEvent>,
    ViewModel() {

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // DATA
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    protected var disposable: CompositeDisposable = CompositeDisposable()
    private val lifecycleSubject:
            BehaviorSubject<ViewModelLifeCycleEvent> = BehaviorSubject.create()

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // CONSTRUCTOR
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    init {
        Timber.tag(this.javaClass.simpleName).i("init")
        lifecycleSubject.onNext(ViewModelLifeCycleEvent.ON_CREATED)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // LIFE CYCLE
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    @CallSuper
    override fun onCleared() {
        Timber.tag(this.javaClass.simpleName).i("onCleared")
        disposable.clear()
        lifecycleSubject.onNext(ViewModelLifeCycleEvent.ON_CLEARED)
        super.onCleared()
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // AUTODISPOSE
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    override fun lifecycle(): Observable<ViewModelLifeCycleEvent> = lifecycleSubject.hide()

    override fun peekLifecycle(): ViewModelLifeCycleEvent? = lifecycleSubject.value
    override fun correspondingEvents(): CorrespondingEventsFunction<ViewModelLifeCycleEvent> {
        return CorrespondingEventsFunction { testLifecycle ->
            when (testLifecycle) {
                ViewModelLifeCycleEvent.ON_CREATED -> ViewModelLifeCycleEvent.ON_CLEARED
                ViewModelLifeCycleEvent.ON_CLEARED -> throw LifecycleEndedException()
            }
        }
    }
}
