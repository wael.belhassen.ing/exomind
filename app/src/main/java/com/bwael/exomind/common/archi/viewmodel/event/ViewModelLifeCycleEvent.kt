package com.bwael.exomind.common.archi.viewmodel.event

/**
 * Represents the viewModel LifeCycle Event in which a ViewModel is in.
 */
enum class ViewModelLifeCycleEvent {
    ON_CREATED, ON_CLEARED
}
