package com.bwael.exomind.common.archi.extension

import com.bwael.exomind.common.archi.extension.StringExtension.replaceLastOccurence
import java.text.NumberFormat
import java.util.*

fun Double.localCurrency(locale: String): String {
    val format = NumberFormat.getCurrencyInstance(Locale.getDefault())
    format.currency = Currency.getInstance(locale)
    return format.format(this)
}

fun Double.localCurrencyWithoutDotsAndWithOneComma(locale: String): String {
    val format = NumberFormat.getCurrencyInstance(Locale.getDefault())
    format.currency = Currency.getInstance(locale)
    return format.format(this).replaceLastOccurence().replace(".", " ").replace("€", "")
}
